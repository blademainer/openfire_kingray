/**
 * openfire_src
 */
package test.com.xiongyingqi.convert;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashSet;

import org.dom4j.Element;
import org.dom4j.Namespace;
import org.jivesoftware.smack.packet.Packet;
import org.junit.Test;

import com.kingray.openfire.plugin.packet.IQElementMapping;
import com.kingray.openfire.plugin.vo.MessageVO;
import com.xiongyingqi.convert.XmlConvert;

/**
 * @author KRXiongYingqi
 * @version 2013-6-17 下午9:55:23
 */
public class TestMessageVOConvert {

	@Test
	public void testConvertMessageVOs(){
		Collection<MessageVO> messageVOs = new HashSet<MessageVO> ();
		for (int i = 0; i < 10; i++) {
			MessageVO messageVO = new MessageVO(i, Packet.nextID(), "熊瑛琪", "zhangjia", null);
			messageVOs.add(messageVO);
		}
		XmlConvert convert = new XmlConvert(IQElementMapping.class);
		Element element = convert.convertVOs(messageVOs);
//		element.addNamespace("", "http://jabber.org/protocol/pubsub#publish");
		System.out.println(element.asXML());
		assertNotNull(element.asXML());
//		Namespace namespace = element.getNamespace();
//		System.out.println(namespace);
		
	}

	public static void main(String[] args) {
		System.out.println(MessageVO.class.getSimpleName());
	}
}
