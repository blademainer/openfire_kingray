/**
 * openfire_src
 */
package test.com.kingray.dao;

import java.util.Date;

import org.junit.Test;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import com.kingray.openfire.plugin.DBConnection;
import com.kingray.openfire.plugin.dao.impl.MessageDAOImpl;
import com.kingray.openfire.plugin.vo.MessageVO;
import com.kingray.openfire.plugin.vo.PageVO;

/**
 * @author XiongYingqi
 * @version 2013-7-4 下午4:46:26
 */
public class TestMessageDAOImpl extends MessageDAOImpl{
	
	public void testGetMessage(){
		super.connection = DBConnection.getConnection();
		
		PageVO pageVO = new PageVO();
		pageVO.setPageNumber(1);
		pageVO.setPageSize(100);
		MessageVO messageVO = new MessageVO();
		messageVO.setMessageFrom("");
		messageVO.setMessageTo("熊瑛琪");
		messageVO.setMessageDateTime(new Date());
		PageVO pageVO2 = new MessageDAOImpl().getMessageHistory(pageVO,
				messageVO, "2012-02-03 00:00:00", "2013-07-30 23:59:59",
				new String[] { "messageDateTimeStr" }, -1);

		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[] { "messageDateTime" });
		JSONArray jsonArray = JSONArray.fromObject(pageVO2, config);
		System.out.println(jsonArray);
	}
	
	@Test
	public void testGetMessageByUserName(){
		super.connection = DBConnection.getConnection();
		
		PageVO pageVO = new PageVO();
		pageVO.setPageNumber(1);
		pageVO.setPageSize(100);
		MessageVO messageVO = new MessageVO();
		messageVO.setMessageFrom("");
		messageVO.setMessageTo("熊瑛琪");
		messageVO.setMessageDateTime(new Date());
		
		PageVO pageVO2 = new MessageDAOImpl().getMessageHistoryByUserName(pageVO, null, "2012-02-03 00:00:00", "2013-07-30 23:59:59", "熊瑛琪", "jy", new String[] { "messageDateTimeStr" }, -1);
		
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[] { "messageDateTime" });
		JSONArray jsonArray = JSONArray.fromObject(pageVO2, config);
		System.out.println(jsonArray.size());
		
//		MessageVO messageVO = new MessageVO(0, "熊瑛琪", "zhangjia", "class com.kingray.openfire.plugin.vo.PageVO", Message.Type.chat + "");
//		new MessageDAOImpl().addMessageHistory(messageVO);
//		
//		PageVO pageVO3 = new PageVO();
//		pageVO3.setPageNumber(1);
//		pageVO3.setPageSize(100);
//		MessageVO messageVO2 = new MessageVO();
//		messageVO2.setMessageFrom("");
//		messageVO2.setMessageTo("熊瑛琪");
//		messageVO2.setMessageDateTime(new Date());
//		PageVO pageVOResult = new MessageDAOImpl().getMessageHistoryByUserName(pageVO3, messageVO2, null, null, "熊瑛琪", null, null, -1);
//		System.out.println(pageVOResult);
		// StringTokenizer tokenizer = new StringTokenizer("aaa,bbbbbb,ccccc");
		// while(tokenizer.hasMoreTokens()){
		// String bodyEmp = tokenizer.nextToken();
		// StringTokenizer commaTokenizer = new StringTokenizer(bodyEmp, ",");
		// while(commaTokenizer.hasMoreTokens()){
		// String bodyRst = commaTokenizer.nextToken();
		// System.out.println(bodyRst);
		// }
		// }
	
	}
}
