/**
 * openfire_src
 */
package test.com.kingray.dao;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.Test;

import com.kingray.openfire.plugin.DBConnection;
import com.kingray.openfire.plugin.dao.MessageDAO;
import com.kingray.openfire.plugin.dao.impl.MessageDAOImpl;
import com.kingray.openfire.plugin.vo.MessageVO;
import com.xiongyingqi.util.CalendarHelper;
import com.xiongyingqi.util.DateHelper;

/**
 * @author KRXiongYingqi
 * @version 2013-6-16 下午10:03:54
 */
public class TestMessageDAO extends MessageDAOImpl{

	/**
	 * <br>2013-7-4 下午4:45:44
	 * @see com.kingray.openfire.plugin.dao.impl.MessageDAOImpl#getConnection()
	 */
	@Override
	public Connection getConnection() {
		return DBConnection.getConnection();
	}
	/**
	 *  ps.setString(5, DateHelper.dateToStrLong(queryDate));
				ps.setString(6, DateHelper.dateToStrLong(queryEndDate));
	 * Test method for {@link com.kingray.openfire.plugin.dao.impl.MessageDAOImpl#getMessageHistoryCountByUserName(java.lang.String)}.
	 */
	@Test
	public void testGetMessageHistoryCountByUserName() {
		MessageDAO messageDAO = new MessageDAOImpl();
		String userName  = "熊瑛琪";
		int count = messageDAO.getMessageHistoryCountByUserName(userName, "admin", new Date(new Date().getTime() - 24 * CalendarHelper.HOUR));
		System.out.println(count);
		assertTrue(count >= 0);
	}
	
	@Test
	public void testGetMessageHistorySummaryByUserName(){
		MessageDAO messageDAO = new MessageDAOImpl();
		String userName  = "熊瑛琪";
		Collection<MessageVO> messageVOs = messageDAO.getMessageHistorySummaryByUserName(userName, "admin", new Date(new Date().getTime() - 24 * CalendarHelper.HOUR));
		System.out.println(messageVOs.size());
		assertTrue(messageVOs.size() >= 0);
	}
	
	@Test
	public void testGetMessageHistoryDetailByUserName(){
		MessageDAO messageDAO = new MessageDAOImpl();
		String userName  = "熊瑛琪";
		Collection<MessageVO> messageVOs = messageDAO.getMessageHistoryDetailByUserName(userName, "", new Date(new Date().getTime() - 24 * CalendarHelper.HOUR));
		System.out.println("testGetMessageHistoryDetailByUserName messageVOs.size() ======== " + messageVOs.size());
		assertTrue(messageVOs.size() >= 0);
	}
	
	@Test
	public void testGetMessageHistoryDetailByMessageIds(){
		Collection<Long> messageIds = new HashSet();
		for (long i = 0; i < 100; i++) {
			messageIds.add(i);
		}
		MessageDAO messageDAO = new MessageDAOImpl();
		Collection<MessageVO> messageVOs = messageDAO.getMessageHistoryDetailByMessageIds(messageIds);
//		System.out.println(messageVOs);
		System.out.println(messageVOs.size());
		assertTrue(messageVOs.size() > 0);
	}
	
	@Test
	public void testGetMessageHistoryDetailBySendMessageIds(){
		MessageDAO messageDAO = new MessageDAOImpl();
		String userName  = "熊瑛琪";
		Collection<MessageVO> messageVOs = messageDAO.getMessageHistorySummaryByUserName(userName, null, new Date(new Date().getTime() - 24 * CalendarHelper.HOUR));
		
		Collection<String> sendMessageIds = new HashSet<String>();
		
		for (Iterator<MessageVO> iterator = messageVOs.iterator(); iterator.hasNext();) {
			MessageVO messageVO = iterator.next();
			sendMessageIds.add(messageVO.getSendMessageId());
		}
		Collection<MessageVO> messageVOsRs = messageDAO.getMessageHistoryDetailBySendMessageIds(sendMessageIds);
		System.out.println(messageVOsRs.size());
	}

}
