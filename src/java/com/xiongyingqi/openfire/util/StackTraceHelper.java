/**
 * openfire_src
 */
package com.xiongyingqi.openfire.util;

/**
 * @author KRXiongYingqi
 * @version 2013-6-19 下午6:21:20
 */
public class StackTraceHelper {
	public static void printStackTrace(){
		Throwable throwable = new Throwable();
		StackTraceElement[] stackTraceElements = throwable.getStackTrace();
		for (int i = 1; i < stackTraceElements.length; i++) {
			StackTraceElement stackTraceElement = stackTraceElements[i];
//			System.out.println(stackTraceElement.getClassName());
//			System.out.println(stackTraceElement.getFileName());
//			System.out.println(stackTraceElement.getMethodName());
//			System.out.println(stackTraceElement.getLineNumber());
			System.out.println("    at " + stackTraceElement.getClassName() + "." +  stackTraceElement.getMethodName() + "(" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + ")");
		}
	}
	public static void main(String[] args) {
		printStackTrace();
//		Long.parseLong("s");
	}
}
