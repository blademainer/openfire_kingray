package com.xiongyingqi.openfire.listener;

import org.xmpp.packet.Message;
/**
 * 消息监听，如果有新消息进来，则会调用该接口 
 * @see org.xmpp.packet.Message
 * @author qi
 *
 */
public interface MessageListener {
	/**
	 * 当有新消息传入时，那么
	 * @param message
	 */
	public void route(Message message);
}
