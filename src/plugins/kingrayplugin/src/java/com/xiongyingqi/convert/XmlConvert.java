/**
 * openfire_src
 */
package com.xiongyingqi.convert;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.kingray.openfire.plugin.packet.KingrayNameSpace;
import com.kingray.openfire.plugin.vo.VO;
import com.xiongyingqi.util.StringHelper;

/**
 * Xml转换工具
 * @author KRXiongYingqi
 * @version 2013-6-17 下午4:44:03
 */
public class XmlConvert {
	private Class annotationClass;
	
	public XmlConvert(Class annotationClass){
		this.annotationClass = annotationClass;
	}
	
	/**
	 * 由Collection<? extends VO>转换为xml，被转换的类必须由有注解类注解
	 * <br>2013-6-17 下午4:44:45
	 * @param collection 消息集合
	 * @return org.dom4j.Element
	 */
	public Element convertVOs(Collection<? extends VO> collection){
		Element element = DocumentHelper.createElement(KingrayNameSpace.QUERY_MESSAGE.getKey());
		if(collection == null){
			return element;
		}
		Collection<? extends VO> copyCollection = new  ArrayList<VO>(collection);// 防止外部修改了数据
		
		for (Iterator iterator = copyCollection.iterator(); iterator.hasNext();) { // 遍历集合
			Object instance =  iterator.next();
			Class instanceClass = instance.getClass();
			
			String elementName = null; 
			if(instanceClass.isAnnotationPresent(annotationClass)){ // 判断VO类是否包含了annotationClass的注解
				Annotation annotation = instanceClass.getAnnotation(annotationClass); // 获取annotationClass的注解
				Method[] methods = annotationClass.getMethods(); // 获取annotationClass的所有方法
				if(methods.length > 1){ // 如果有多个注解
					for (int i = 0; i < methods.length; i++) {
						Method method = methods[i];
						String methodName = method.getName(); // 注解方法的方法名
						
						if("value".equals(methodName)){ // 是否为value方法
							try {
								elementName = (String) method.invoke(annotation);
							} catch (IllegalArgumentException e) {
							} catch (IllegalAccessException e) {
							} catch (InvocationTargetException e) {
							}
							break;
						}
					}
				} else if(methods.length == 1){
					Method method = methods[0];
					String methodName = method.getName(); // 注解方法的方法名
					
					if("value".equals(methodName)){ // 是否为value方法
						try {
							elementName = (String) method.invoke(annotation);
						} catch (IllegalArgumentException e) {
						} catch (IllegalAccessException e) {
						} catch (InvocationTargetException e) {
						}
					}
				}
				
			}
			if(StringHelper.nullOrEmpty(elementName)){
				elementName = instance.getClass().getSimpleName();
			}
			Element messageElement = DocumentHelper.createElement(elementName); // 创建节点
			
//			Class clazz = IQElementMapping.class;
			Field[] fields = instance.getClass().getDeclaredFields();
			for (int i = 0; i < fields.length; i++) { // 遍历每个字段
				Field field = fields[i];
				if(field.isAnnotationPresent(annotationClass)){ // 检测是否为IQElementMapping的注解类型
					Annotation annotation = field.getAnnotation(annotationClass);
					Method[] methods = annotationClass.getDeclaredMethods(); // 获取注解类的所有方法
					for (int j = 0; j < methods.length; j++) {
						Method annoutationMethod = methods[j];
						String fieldName = null;
						try {
							fieldName = annoutationMethod.invoke(annotation) + ""; // 获取注解方法的返回值
						} catch (IllegalArgumentException e1) {
							e1.printStackTrace();
						} catch (IllegalAccessException e1) {
							e1.printStackTrace();
						} catch (InvocationTargetException e1) {
							e1.printStackTrace();
						}
//						annotation.value();
						String fieldValue = null; // 被注解的字段的值
						try {
							String instanceFieldName = field.getName();
							String methodName = "get" + instanceFieldName.substring(0, 1).toUpperCase() + instanceFieldName.substring(1); // 使用bean字段的取值方法
							Method method = instance.getClass().getDeclaredMethod(methodName);
							Object returnObject = method.invoke(instance);
							if(returnObject != null){
								fieldValue = returnObject + "";
							}
//						fieldValue = (String) field.get(messageVO);
						} catch (IllegalArgumentException e) {
						} catch (IllegalAccessException e) {
						} catch (SecurityException e) {
						} catch (NoSuchMethodException e) {
						} catch (InvocationTargetException e) {
						}
						if(StringHelper.notNullAndNotEmpty(fieldValue)){
							messageElement.addAttribute(fieldName, fieldValue);  // 为每个字段设置属性
						}
					}
				}
			}
			
			Method[] methods = instanceClass.getDeclaredMethods();
			for (int i = 0; i < methods.length; i++) {
				Method method = methods[i];
				if(method.isAnnotationPresent(annotationClass)){
					Annotation annotation = method.getAnnotation(annotationClass);
					Method[] annotationMethods = annotationClass.getDeclaredMethods(); // 获取注解类的所有方法
					for (int j = 0; j < annotationMethods.length; j++) {
						Method annotationMethod = annotationMethods[j];
						String fieldName = null;
						try {
							fieldName = annotationMethod.invoke(annotation) + ""; // 获取注解方法的返回值
						} catch (IllegalArgumentException e1) {
							e1.printStackTrace();
						} catch (IllegalAccessException e1) {
							e1.printStackTrace();
						} catch (InvocationTargetException e1) {
							e1.printStackTrace();
						}
						String fieldValue = null; // 被注解的字段的值
						try {
							Object returnObject = method.invoke(instance);
							if(returnObject != null){
								fieldValue = returnObject + "";
							}
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
						if(StringHelper.notNullAndNotEmpty(fieldValue)){
							messageElement.addAttribute(fieldName, fieldValue);  // 为每个字段设置属性
						}
					}
				}
			}
			
			element.add(messageElement);
		}
		return element;
	}
}
