package com.kingray.openfire.plugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import javax.naming.spi.ResolveResult;

public class DBConnection {
	private static DBConnection dbConnection = new DBConnection();
	private Set<Connection> connections;
	/**
	 * 最小连接数
	 */
	private int minConnectionNumber;
	/**
	 * 最大连接数
	 */
	private int maxConnectionNumber;

	/**
	 * 默认最小5个连接，最大20个连接
	 */
	private DBConnection() {
		minConnectionNumber = 5;
		maxConnectionNumber = 20;
	}

	/**
	 * 单例模式
	 * 
	 * @return
	 */
	public static DBConnection getInstance() {
		return dbConnection;
	}
	
	public static Connection getConnection(){
//		if(connections == null){ // 初始化集合
//			connections = new LinkedHashSet<Connection>();
//		}
//		Iterator<Connection> iterator = connections.iterator();
//		for (Iterator iterator2 = iterator; iterator2.hasNext();) {
//			Connection connection = (Connection) iterator2.next();
//			try {
//				if(connection.isClosed()){
//					connection = createConnection();
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
		
		Connection connection = createConnection();
//		try {
//			connection.isClosed();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		return connection;
	}
	
	
	private static Connection createConnection() {
		Connection conn = null;
		try {
			String driver = "com.mysql.jdbc.Driver";
			// URL指向要访问的数据库名scutcs
			String url = "jdbc:mysql://10.188.199.3:3306/openfire";
			// MySQL配置时的用户名
			String user = "root";
			// Java连接MySQL配置时的密码
			String password = "kry123456";
			try {
				// 加载驱动程序
				Class.forName(driver);
				conn = DriverManager.getConnection(url, user, password);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			// 连续数据库
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public int getMinConnectionNumber() {
		return minConnectionNumber;
	}

	public void setMinConnectionNumber(int minConnectionNumber) {
		this.minConnectionNumber = minConnectionNumber;
	}

	public int getMaxConnectionNumber() {
		return maxConnectionNumber;
	}

	public void setMaxConnectionNumber(int maxConnectionNumber) {
		this.maxConnectionNumber = maxConnectionNumber;
	}

	public static void main(String[] args) {
		String ADD_MESSAGE_HISTORY_SQL = "insert into kr_message_history(message_from, message_to, message_body) values(?, ?, ?)";
		PreparedStatement preparedStatement = null;
		Connection connection = DBConnection.getInstance().createConnection();
		try {
			preparedStatement = connection.prepareStatement(ADD_MESSAGE_HISTORY_SQL, preparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, "a");
			preparedStatement.setString(2, "b");
			preparedStatement.setString(3, "c");
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			while(rs.next()){
				int id = rs.getInt(1);
				System.out.println(id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
