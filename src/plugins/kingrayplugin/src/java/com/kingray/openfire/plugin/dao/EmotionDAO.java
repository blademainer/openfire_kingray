/**
 * openfire_src
 */
package com.kingray.openfire.plugin.dao;

import java.util.Collection;

import com.kingray.openfire.plugin.vo.Emotion;

/**
 * 表情数据库类
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-5 上午11:34:08
 */
public interface EmotionDAO extends DAO{
	/**
	 * 获取所有的共享表情
	 * <br>2013-9-5 下午2:18:59
	 * @return 所有表情集合
	 */
	public Collection<Emotion> getAllEmotions();
	
	/**
	 * 获取单个表情
	 * <br>2013-9-5 下午2:19:23
	 * @param emotionId
	 * @return 包含emotionId值的表情对象
	 */
	public Emotion getEmotion(String emotionId);
	
	/**
	 * 添加共享表情
	 * <br>2013-9-5 下午2:19:32
	 * @param emotion
	 * @return 添加成功的对象
	 */
	public Emotion addEmotion(Emotion emotion);
	
	/**
	 * 查询表情的数量
	 * <br>2013-9-10 下午8:50:50
	 * @return 表情数
	 */
	public int getEmotionSize();
	
	/**
	 * 获取概要的表情信息
	 * <br>2013-9-10 下午9:18:55
	 * @return 概要的表情信息
	 */
	public Collection<Emotion> getSummaryEmotions();
	
	/**
	 * 根据Emotion集合查询具体的数据
	 * <br>2013-9-10 下午9:40:36
	 * @return
	 */
	public Collection<Emotion> getEmotionsByIds(Collection<Emotion> emotions);
	
}
