package com.kingray.openfire.plugin.vo;

import java.util.Date;

import com.kingray.openfire.plugin.packet.IQElementMapping;
import com.xiongyingqi.util.DateHelper;

/**
 * 消息实体，用于存储传递时用到的查询条件或返回结果
 * @author KRXiongYingqi
 * @version 2013-6-20 下午2:14:47
 */
@IQElementMapping("message")
public class MessageVO implements VO {
	@IQElementMapping("messageId")
	private long messageId;
	
	@IQElementMapping("sendMessageId")
	private String sendMessageId;
	
	@IQElementMapping("messageFrom")
	private String messageFrom;
	
	@IQElementMapping("messageTo")
	private String messageTo;
	
	@IQElementMapping("messageSubject")
	private String messageSubject;
	
	@IQElementMapping("messageBody")
	private String messageBody;
	
//	@IQElementMapping("messageDateTime") 由于messageDateTimeStr能转换为messageDateTime，故本字段舍弃
	private Date messageDateTime;
	
	@IQElementMapping("messageDateTimeStr")
	private String messageDateTimeStr;
	
	@IQElementMapping("messageType")
	private String messageType;
	
	public MessageVO(){
	}
	public MessageVO(long messageId, String sendMessageId, String messageFrom, String messageTo, String messageBody){
		this.messageId = messageId;
		this.sendMessageId = sendMessageId;
		this.messageFrom = messageFrom;
		this.messageTo = messageTo;
		this.messageBody = messageBody;
	}
	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return messageType;
	}
	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	/**
	 * @return the messageSubject
	 */
	public String getMessageSubject() {
		return messageSubject;
	}
	/**
	 * @param messageSubject the messageSubject to set
	 */
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
	/**
	 * @return the messageId
	 */
	public long getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return the sendMessageId
	 */
	public String getSendMessageId() {
		return sendMessageId;
	}
	/**
	 * @param sendMessageId the sendMessageId to set
	 */
	public void setSendMessageId(String sendMessageId) {
		this.sendMessageId = sendMessageId;
	}
	/**
	 * @return the messageFrom
	 */
	public String getMessageFrom() {
		return messageFrom;
	}
	/**
	 * @param messageFrom the messageFrom to set
	 */
	public void setMessageFrom(String messageFrom) {
		if(messageFrom == null) messageFrom = "";
		this.messageFrom = messageFrom;
	}
	/**
	 * @return the messageTo
	 */
	public String getMessageTo() {
		return messageTo;
	}
	/**
	 * @param messageTo the messageTo to set
	 */
	public void setMessageTo(String messageTo) {
		if(messageTo == null) messageTo = "";
		this.messageTo = messageTo;
	}
	/**
	 * @return the messageBody
	 */
	public String getMessageBody() {
		return messageBody;
	}
	/**
	 * @param messageBody the messageBody to set
	 */
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	/**
	 * @return the messageDateTime
	 */
	public Date getMessageDateTime() {
		return messageDateTime;
	}
	/**
	 * @param messageDateTime the messageDateTime to set
	 */
	public void setMessageDateTime(Date messageDateTime) {
		this.messageDateTimeStr = DateHelper.FORMATTER_LONG.format(messageDateTime);
		this.messageDateTime = messageDateTime;
	}
	/**
	 * @return the messageDateTimeStr
	 */
	public String getMessageDateTimeStr() {
		return messageDateTimeStr;
	}
	/**
	 * @param messageDateTimeStr the messageDateTimeStr to set
	 */
	public void setMessageDateTimeStr(String messageDateTimeStr) {
		this.messageDateTime = DateHelper.strToDateLong(messageDateTimeStr);
		this.messageDateTimeStr = messageDateTimeStr;
	}
	@Override
	public String toString() {
		return this.getClass() + "@" + hashCode() + ": messageId: " + messageId + ", sendMessageId: " 
				+ sendMessageId + ", messageFrom: " + messageFrom + ", messageTo: " + messageTo + ", messageBody: " 
				+ messageBody + ", messageDateTime: " + messageDateTime;
	}
}
