package com.kingray.openfire.plugin.vo;

import java.util.Collection;

import org.jivesoftware.openfire.group.Group;

public class GroupVO implements VO{
	private String groupName;
	private Collection<UserStatusVO> userStatusVOs;
	
	public GroupVO(Group group, Collection<UserStatusVO> userStatusVOs){
		this.groupName = group.getName();
		this.userStatusVOs = userStatusVOs;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Collection<UserStatusVO> getUserStatusVOs() {
		return userStatusVOs;
	}

	public void setUserStatusVOs(Collection<UserStatusVO> userStatusVOs) {
		this.userStatusVOs = userStatusVOs;
	}
	
}
