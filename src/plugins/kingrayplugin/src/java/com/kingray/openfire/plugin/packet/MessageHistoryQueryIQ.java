package com.kingray.openfire.plugin.packet;

import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.xmpp.packet.IQ;

import com.xiongyingqi.util.DateHelper;

/**
 * 消息历史查询IQ
 * @author XiongYingqi
 *
 */
public class MessageHistoryQueryIQ extends IQ{
	private static final KingrayNameSpace QUERY_MESSAGE = KingrayNameSpace.QUERY_MESSAGE;
	
	private String userName; // 查询用户
	private String relationUserName;  // 关联用户
	private Date messageDateTime;  // 消息日期
	private String messageDateTimeStr; //  消息日期字符串
	

	
	/**
	 * @return the messageDateTimeStr
	 */
	public String getMessageDateTimeStr() {
		return messageDateTimeStr;
	}

	/**
	 * @param messageDateTimeStr the messageDateTimeStr to set
	 */
	public void setMessageDateTimeStr(String messageDateTimeStr) {
		this.messageDateTimeStr = messageDateTimeStr;
		this.messageDateTime = DateHelper.strToDate(messageDateTimeStr); // 字符串转换为日期格式
	}

	/**
	 * @return the relationUserName
	 */
	public String getRelationUserName() {
		return relationUserName;
	}

	/**
	 * @param relationUserName the relationUserName to set
	 */
	public void setRelationUserName(String relationUserName) {
		this.relationUserName = relationUserName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the messageDateTime
	 */
	public Date getMessageDateTime() {
		return messageDateTime;
	}

	/**
	 * @param messageDateTime the messageDateTime to set
	 */
	public void setMessageDateTime(Date messageDateTime) {
		this.messageDateTime = messageDateTime;
		this.messageDateTimeStr= DateHelper.FORMATTER_SHORT.format(messageDateTime); // 转换为短日期格式
	}

	private String getNameSpace(){
		return QUERY_MESSAGE.getValue();
	}
	
	
	public String getChildElementXML() {
		Document document = DocumentHelper.createDocument(); // 创建document对象
		Element root = document.addElement(QUERY_MESSAGE.getKey()); // 添加自定义节点
		root.addNamespace("", getNameSpace()); // 加入命名空间
		root.addAttribute("userName", userName); // 加入查询人
		if(relationUserName != null && !"".equals(relationUserName)){
			root.addAttribute("relationUserName", relationUserName); // 添加关联用户
		}
		if(messageDateTime != null){
			root.addAttribute("messageDateTime", messageDateTimeStr); // 添加查询日期
		}
		return  root.asXML();
	}
	
	public static void main(String[] args) {
		MessageHistoryQueryIQ messageHistoryIQ = new MessageHistoryQueryIQ();
		System.out.println(messageHistoryIQ.toXML());
		
//		try {
//			for (Method method : Class.forName("com.kingray.spark.plugin.packet.IQType").getMethods()) {
//				
//			}
//		} catch (SecurityException e) {
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//		 try {
//			Class clazz = Class.forName("com.kingray.spark.plugin.packet.MessageHistoryIQ");
//			boolean isAnnotation = clazz.isAnnotationPresent(IQType.class);
//			if(isAnnotation){
//				IQType iqtype = (IQType) clazz.getAnnotation(IQType.class);
//				System.out.println(iqtype.value().getValue());
//			}
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
	}

}
