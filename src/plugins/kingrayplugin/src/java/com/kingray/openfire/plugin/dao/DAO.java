package com.kingray.openfire.plugin.dao;

public interface DAO {
	/**
	 * 消息总数sql语句
	 */
	public String RECORD_COUNT_SQL = "select count(*) from #SQL#";
	
	/**
	 * 分页查询语句
	 * 第一个参数为跳过n条记录，即从 n+1 条记录开始获取
	 * 第二个参数为获取的记录数
	 */
	public String PAGE_SQL = "select s.* from (#SQL#) s limit ?, ?"; 
}
