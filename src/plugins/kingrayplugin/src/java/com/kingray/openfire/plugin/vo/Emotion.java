/**
 * spark_src
 */
package com.kingray.openfire.plugin.vo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.dom4j.Element;

import com.kingray.openfire.plugin.packet.IQElementMapping;
import com.xiongyingqi.convert.XmlConvert;
import com.xiongyingqi.util.Base64;
import com.xiongyingqi.util.FileHelper;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午4:24:29
 */
@IQElementMapping("emotion")
public class Emotion implements VO{
	/**
	 * 服务器内部标识
	 */
	@IQElementMapping("id")
	private int id;
	
	/**
	 * 图标所指向的文件
	 */
	private File emotionFile;
	
	/**
	 * 图标的显示字符
	 */
	@IQElementMapping("emotionString")
	private String emotionString;
	
	/**
	 * 系统全局标识
	 */
	@IQElementMapping("emotionId")
	private String emotionId;
	
	
	/**
	 * int
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * int
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * String
	 * @return the emotionId
	 */
	public String getEmotionId() {
		return emotionId;
	}

	/**
	 * String
	 * @param emotionId the emotionId to set
	 */
	public void setEmotionId(String emotionId) {
		this.emotionId = emotionId;
	}

	/**
	 * 将图标转换为Base64字符串
	 * <br>2013-9-4 下午4:33:22
	 * @return
	 */
	@IQElementMapping("base64Data")
	public String toBase64(){
		String base64 = null;
		try {
			if(emotionFile != null){
				byte[] data = FileHelper.readFileToBytes(emotionFile);
				base64 = Base64.encodeBytes(data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return base64;
	}
	
	/**
	 * File
	 * @return the emotionFile
	 */
	public File getEmotionFile() {
		return emotionFile;
	}
	/**
	 * File
	 * @param emotionFile the emotionFile to set
	 */
	public void setEmotionFile(File emotionFile) {
		this.emotionFile = emotionFile;
	}
	/**
	 * String
	 * @return the emotionString
	 */
	public String getEmotionString() {
		return emotionString;
	}
	/**
	 * String
	 * @param emotionString the emotionString to set
	 */
	public void setEmotionString(String emotionString) {
		this.emotionString = emotionString;
	}
	
	/**
	 * 以emotionId作为唯一标识
	 * <br>2013-9-5 下午5:51:15
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return emotionId.hashCode();
	}
	
	/**
	 * <br>2013-9-5 下午5:51:51
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof Emotion && obj.hashCode() == this.hashCode()){
			return true;
		} else {
			return false;
		}
	}
	
	public static void main(String[] args) {
//		XmlConvert convert  = new XmlConvert(IQElementMapping.class);
//		Emotion emotion = new Emotion();
//		emotion.setEmotionString("aaa");
//		emotion.setEmotionFile(new File("C:/Users/瑛琪/Desktop/a.gif"));
//		Collection<Emotion> emotions = new ArrayList<Emotion>();
//		emotions.add(emotion);
//		Element element = convert.convertVOs(emotions);
//		System.out.println(element.asXML());
		
		Emotion emotion = new Emotion();
		emotion.setEmotionId("aaa");
		Emotion emotion2 = new Emotion();
		emotion2.setEmotionId("aaa");
		System.out.println(emotion.equals(emotion2));
	}
	
}
