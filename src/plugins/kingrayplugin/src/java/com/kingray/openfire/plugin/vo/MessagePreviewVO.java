package com.kingray.openfire.plugin.vo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MessagePreviewVO {
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private String messageFrom;
	/**
	 * 最新消息预览
	 */
	private String lastMessagePreview;
	private String messageDateTimeStr;
	private int messageCount;
	private Date messageDateTime;
	
	public String getMessageFrom() {
		return messageFrom;
	}
	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	public String getLastMessagePreview() {
		return lastMessagePreview;
	}
	public void setLastMessagePreview(String lastMessagePreview) {
		this.lastMessagePreview = lastMessagePreview;
	}
	public String getMessageDateTimeStr() {
		return messageDateTimeStr;
	}
	public int getMessageCount() {
		return messageCount;
	}
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}
	public Date getMessageDateTime() {
		return messageDateTime;
	}
	public void setMessageDateTime(Date messageDateTime) {
		this.messageDateTimeStr = DATE_FORMAT.format(messageDateTime);
		this.messageDateTime = messageDateTime;
	}
	
}
