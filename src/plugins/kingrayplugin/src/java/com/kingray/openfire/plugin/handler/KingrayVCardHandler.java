/**
 * openfire_src
 */
package com.kingray.openfire.plugin.handler;

import org.dom4j.Element;
import org.jivesoftware.openfire.vcard.VCardListener;

/**
 * VCard处理中心
 * @author XiongYingqi
 * @version 2013-7-4 上午10:52:48
 */
public class KingrayVCardHandler implements VCardListener{

	/**
	 * <br>2013-7-4 上午10:54:10
	 * @see org.jivesoftware.openfire.vcard.VCardListener#vCardCreated(java.lang.String, org.dom4j.Element)
	 */
	@Override
	public void vCardCreated(String username, Element vCard) {
		
	}

	/**
	 * <br>2013-7-4 上午10:54:10
	 * @see org.jivesoftware.openfire.vcard.VCardListener#vCardUpdated(java.lang.String, org.dom4j.Element)
	 */
	@Override
	public void vCardUpdated(String username, Element vCard) {
		
	}

	/**
	 * <br>2013-7-4 上午10:54:10
	 * @see org.jivesoftware.openfire.vcard.VCardListener#vCardDeleted(java.lang.String, org.dom4j.Element)
	 */
	@Override
	public void vCardDeleted(String username, Element vCard) {
		
	}


}
