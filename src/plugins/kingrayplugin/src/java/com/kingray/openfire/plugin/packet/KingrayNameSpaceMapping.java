package com.kingray.openfire.plugin.packet;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * IQ命名空间注解
 * @author KRXiongYingqi
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface KingrayNameSpaceMapping {
	/**
	 * 用于指定IQ的命名空间值
	 * @return
	 */
	public KingrayNameSpace value();
}
