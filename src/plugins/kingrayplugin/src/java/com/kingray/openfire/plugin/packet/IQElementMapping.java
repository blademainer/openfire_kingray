package com.kingray.openfire.plugin.packet;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * IQ映射注解，用于注解实体类的字段，帮助转换器转换为xml对象
 * @author KRXiongYingqi
 * @version 2013-6-17 上午10:44:44
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE, ElementType.METHOD})
public @interface IQElementMapping {
	public String value();
}
