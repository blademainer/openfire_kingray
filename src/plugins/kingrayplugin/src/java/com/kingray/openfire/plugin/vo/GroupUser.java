/**
 * openfire_src
 */
package com.kingray.openfire.plugin.vo;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 分组与用户的关联
 * @author 瑛琪
 * @version 2013-7-17 下午4:28:58
 */
public class GroupUser {
	private String groupName;
	private String userName;
	private String name;
	private String password;
	private int sortNumber;

	public GroupUser(String groupName, String userName, String name,
			String password, int sortNumber) {
		this.groupName = groupName;
		this.userName = userName;
		this.name = name;
		this.password = password;
		this.sortNumber = sortNumber;
	}

	
	public int getSortNumber() {
		return sortNumber;
	}


	public void setSortNumber(int sortNumber) {
		this.sortNumber = sortNumber;
	}


	/**
	 * @return
	 */
	public String getGroupName() {
		try {
			return URLEncoder.encode(new String(groupName), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUserName() {
		try {
			return URLEncoder.encode(new String(userName), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		try {
			return URLEncoder.encode(new String(name), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "userName: " + userName + ", name: " + name + ", group: "
				+ groupName + ", password: " + password;
	}

}
