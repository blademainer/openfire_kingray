package com.kingray.openfire.plugin.dao;

import java.util.Collection;
import java.util.Date;

import com.kingray.openfire.plugin.vo.MessageVO;
import com.kingray.openfire.plugin.vo.PageVO;

public interface MessageDAO extends DAO {
	/**
	 * 插入消息记录
	 * @param messageVO
	 * @return
	 */
	public boolean addMessageHistory(MessageVO messageVO);
	/**
	 * 分页获取历史消息
	 * @param inputPageVO 查询分页实体
	 * @param inputMessageVO 消息关键字
	 * @param dateFrom 查找开始时间
	 * @param dateTO 查找截至日期
	 * @param orderBys 排序关键字
	 * @param asc 升序
	 * @return PageVO
	 */
	public PageVO getMessageHistory(PageVO inputPageVO, MessageVO inputMessageVO, String dateFrom, String dateTO, String[] orderBys, int asc);

	/**
	 * 	 * 根据用户名分页获取历史消息
	 * <br>2013-6-17 下午3:44:45
	 * @param inputPageVO 查询分页实体
	 * @param inputMessageVO 消息关键字
	 * @param dateFrom 查找开始时间
	 * @param relationUserName 
	 * @param userName 
	 * @param dateTO 查找截至日期
	 * @param orderBys 排序关键字
	 * @param asc 升序
	 * @return com.kingray.openfire.plugin.vo.PageVO
	 */
	public PageVO getMessageHistoryByUserName(PageVO inputPageVO,
			MessageVO messageVO, String dateFrom, String dateTo,
			String userName, String relationUserName, String[] orderBys, int asc);
	
	/**
	 * 根据消息id获取详细消息内容
	 * @param messageId
	 * @return
	 */
	public MessageVO getMessageHistoryByMessageId(long messageId);
	
	/**
	 * 根据用户名查询消息总条数
	 * <br>2013-6-17 下午3:56:49
	 * @param userName 用户名，可能是发送者，也可能是接收者
	 * @param relationUserName 关联用户名，对应用户名可能是发送者也可能是接收者
	 * @param queryDate 要查询的日期（yyyy-MM-dd），如果为空，则表示要查询所有时间的消息
	 * @return int
	 */
	public int getMessageHistoryCountByUserName(String userName, String relationUserName, Date queryDate);
	
	/**
	 * 根据用户名、关联用户、查询日期来查询用户的历史消息预览
	 * <br>2013-6-17 下午3:39:12
	 * @param userName 用户名，可能是发送者，也可能是接收者
	 * @param relationUserName 关联用户名，对应用户名，如果userName为发送者则relationUserName为接受者，如果userName为发送者则relationUserName为接受者
	 * @param queryDate 要查询的日期（yyyy-MM-dd），如果为空，则表示要查询所有时间的消息
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> getMessageHistorySummaryByUserName(String userName,
			String relationUserName, Date queryDate);
	
	/**
	 * 根据用户名、关联用户、查询日期来查询用户的历史消息详情
	 * <br>2013-6-18 上午9:59:53
	 * @param userName 用户名，可能是发送者，也可能是接收者
	 * @param relationUserName 关联用户名，对应用户名，如果userName为发送者则relationUserName为接受者，如果userName为发送者则relationUserName为接受者
	 * @param queryDate 要查询的日期（yyyy-MM-dd），如果为空，则表示要查询所有时间的消息
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> getMessageHistoryDetailByUserName(
			String userName, String relationUserName, Date queryDate);
	/**
	 * 根据消息ID查询消息详情
	 * <br>2013-6-18 上午10:50:10
	 * @param messageIds
	 * @return
	 */
	public Collection<MessageVO> getMessageHistoryDetailByMessageIds(
			Collection<Long> messageIds);
	/**
	 * 根据发送消息ID查询消息详情
	 * <br>2013-6-18 上午10:50:38
	 * @param sendMessageIds
	 * @return
	 */
	public Collection<MessageVO> getMessageHistoryDetailBySendMessageIds(
			Collection<String> sendMessageIds);
}
