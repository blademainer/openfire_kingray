/**
 * spark_src
 */
package com.kingray.openfire.plugin.packet;

import java.util.Collection;

import org.dom4j.Element;
import org.xmpp.packet.IQ;

import com.kingray.openfire.plugin.vo.Emotion;

/**
 * 与服务器交流数据的实体
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午5:17:26
 */
public class EmotionIQ extends IQ{
	private EmotionActionType actionType;
	
	private Collection<Emotion> emotions;
	
	private int emotionCount;
	
	/**
	 * int
	 * @return the emotionCount
	 */
	public int getEmotionCount() {
		return emotionCount;
	}

	/**
	 * int
	 * @param emotionCount the emotionCount to set
	 */
	public void setEmotionCount(int emotionCount) {
		this.emotionCount = emotionCount;
	}

	/**
	 * EmotionActionType
	 * @return the actionType
	 */
	public EmotionActionType getActionType() {
		return actionType;
	}

	/**
	 * EmotionActionType
	 * @param actionType the actionType to set
	 */
	public void setActionType(EmotionActionType actionType) {
		this.actionType = actionType;
	}

	/**
	 * Collection<Emotion>
	 * @return the emotions
	 */
	public Collection<Emotion> getEmotions() {
		return emotions;
	}

	/**
	 * Collection<Emotion>
	 * @param emotions the emotions to set
	 */
	public void setEmotions(Collection<Emotion> emotions) {
		this.emotions = emotions;
	}


	/**
	 * <br>2013-9-5 下午2:39:19
	 * @see org.xmpp.packet.IQ#getChildElement()
	 */
	@Override
	public Element getChildElement() {
		return super.getChildElement();
	}
	
	public enum EmotionActionType{
		/**
		 * 添加表情
		 */
		ADD_EMOTION,
		/**
		 * 查询总条数
		 */
		QUERY_COUNT, 
		/**
		 * 查询概要ID
		 */
		QUERY_SUMMARY,
		/**
		 * 查询详细消息
		 */
		QUERY_DETAIL, 
	}
}
