//package com.kingray.openfire.plugin.handler;
//
//import org.jivesoftware.openfire.IQHandlerInfo;
//import org.jivesoftware.openfire.XMPPServer;
//import org.jivesoftware.openfire.auth.UnauthorizedException;
//import org.jivesoftware.openfire.handler.IQHandler;
//import org.jivesoftware.openfire.session.ClientSession;
//import org.jivesoftware.openfire.session.LocalSession;
//import org.xmpp.packet.IQ;
//import org.xmpp.packet.JID;
//
//import com.kingray.openfire.plugin.packet.KingrayNameSpace;
//
///**
// * 服务类，用于处理客户端的数据查询等请求
// * @author XiongYingqi
// * 
// */
//public class KingrayMessageHistoryHandler extends IQHandler{
//	private static final KingrayNameSpace QUERY_MESSAGE = KingrayNameSpace.QUERY_MESSAGE; // 命名空间
//	
//	public KingrayMessageHistoryHandler() {
//		super(QUERY_MESSAGE.getValue());
//	}
//
//	@Override
//	public IQ handleIQ(IQ packet) throws UnauthorizedException {
//		JID from = packet.getFrom();
//		
//		ClientSession session = XMPPServer.getInstance().getSessionManager().getSession(from);
//		return null;
//	}
//
//	@Override
//	public IQHandlerInfo getInfo() {
//		return null;
//	}
//}
