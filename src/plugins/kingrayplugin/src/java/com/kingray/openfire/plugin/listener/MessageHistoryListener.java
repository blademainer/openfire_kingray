package com.kingray.openfire.plugin.listener;

import org.dom4j.Attribute;
import org.dom4j.Element;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;

import com.kingray.openfire.plugin.dao.MessageDAO;
import com.kingray.openfire.plugin.dao.impl.MessageDAOImpl;
import com.kingray.openfire.plugin.vo.MessageVO;
import com.xiongyingqi.util.DomainHelper;
/**
 * 处理openfire用户消息
 * @author qi
 *
 */
public class MessageHistoryListener implements PacketInterceptor{ //, MessageListener
	public static final Logger log = LoggerFactory.getLogger(MessageHistoryListener.class);
	private MessageDAO messageDAO = new MessageDAOImpl();
	
	/**
	 * 通过实现PacketInterceptor获取消息
	 * @param packet
	 * @param session
	 * @param incoming
	 * @param processed
	 * @throws PacketRejectedException
	 */
	@Override
	public void interceptPacket(Packet packet, Session session,
			boolean incoming, boolean processed) throws PacketRejectedException {
		if(packet != null){
			log.debug("packet: " + packet);
			log.debug("incoming: " + incoming);
			log.debug("processed: " + processed);
			log.debug("session: " + session);
			
//			System.out.println("packet: " + packet);
//			System.out.println("incoming: " + incoming);
//			System.out.println("processed: " + processed);
//			System.out.println("session: " + session);
		}
		Element messageElement = packet.getElement();
		if(messageElement != null && "message".equals(messageElement.getName()) && incoming == true && processed == true){ // incoming == true && processed == true 是为了捕获已经处理并且进入系统的消息
			try{
				Element bodyElement = messageElement.element("body");// body节点
				if(bodyElement != null){
					String from = DomainHelper.removeDomain(messageElement.attribute("from").getText());
					String to = DomainHelper.removeDomain(messageElement.attribute("to").getText());
					String type = "";
					try {
						type = DomainHelper.removeDomain(messageElement.attribute("type").getText());
					} catch (Exception e) {
					}
					String id = "";
					Attribute attr = messageElement.attribute("id");
					if(attr != null){
						id = attr.getText();
					}
					String body = bodyElement.getText();
					if(body != null && !"".equals(body)){
						MessageVO messageVO = new MessageVO(0L, id, from, to, body);
						messageVO.setMessageType(type);
						Element subjectElement = messageElement.element("subject");
						String subject = null;
						if(subjectElement != null){
							subject = subjectElement.getText();
							messageVO.setMessageSubject(subject);
						}
						messageDAO.addMessageHistory(messageVO);
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}
	
	
	
	/**
	 * 实现MessageListener接口方法
	 * @param message
	 */
	public void route(Message message) {
		Element messageElement = message.getElement();
		if(messageElement != null && "message".equals(messageElement.getName())){
			try{
				Element bodyElement = messageElement.element("body");// body节点
				if(bodyElement != null){
					String from = DomainHelper.removeDomain(messageElement.attribute("from").getText());
					String to = DomainHelper.removeDomain(messageElement.attribute("to").getText());
					String id = messageElement.attribute("id").getText();
					String type = "";
					try {
						type = DomainHelper.removeDomain(messageElement.attribute("type").getText());
					} catch (Exception e) {
					}
					String body = bodyElement.getText();
					log.debug("from: " + from);
					log.debug("to: " + to);
					if(body != null && !"".equals(body)){
						MessageVO messageVO = new MessageVO(0L, id, from, to, body);
						messageVO.setMessageType(type);
						messageDAO.addMessageHistory(messageVO);
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}
	
	
	
	public static void main(String[] args) {
//		String addr = "admin@xiongyingqi/Spark 2.6.3";
//		addr = new MessageHistoryHandle().removeDomain(addr);
//		System.out.println(addr);
		
		Message message = new Message();
		message.setID("aaa");
		message.setType(Message.Type.chat);
		message.setFrom("from");
		message.setFrom("to");
		message.setSubject("aaa");
		message.setBody("body");
		System.out.println(message);
	}
}
