package com.kingray.openfire.plugin.vo;

import java.util.Date;

import org.jivesoftware.openfire.user.User;
import org.xmpp.packet.Presence;
import org.xmpp.packet.Presence.Show;
import org.xmpp.packet.Presence.Type;

import com.kingray.openfire.plugin.KingrayServicePlugin;
import com.kingray.openfire.plugin.servlet.PresenceServiceServlet;

public class UserStatusVO implements VO{
	
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 用户姓名
	 */
	private String name;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 创建日期
	 */
    private Date creationDate;
    /**
     * 最后修改日期
     */
    private Date modificationDate;
    /**
     * 用户状态
     */
    private String status;
    /**
     * 状态表示
     */
    private String show;
    /**
     * 优先级
     */
    private int priority;
    /**
     * 状态图片
     */
    private String statusImageURL;
    
	public UserStatusVO(){}
	/**
	 * 传入用户和状态的构造参数
	 * @param user
	 * @param presence
	 */
	public UserStatusVO(User user, Presence presence){
		this.userName = user.getUsername();
		this.name = user.getName();
		this.email = user.getEmail();
		this.creationDate = user.getCreationDate();
		this.modificationDate = user.getModificationDate();
		if(presence != null){
			this.status = presence.getStatus();
			if(presence.getShow() != null){
				this.show = presence.getShow().toString();
			} else { // 无显示则为在线
				this.show = "available";
			}
			this.priority = presence.getPriority();
		} else {
			this.status = "离线";
			this.show = "offline";
			this.priority = 0;
		}
		this.statusImageURL = PresenceServiceServlet.STATUS_IMAGE_REQUEST_PATH + "&show=" + show;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getShow() {
		return show;
	}
	public void setShow(String show) {
		this.show = show;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getStatusImageURL() {
		return statusImageURL;
	}
	public void setStatusImageURL(String statusImageURL) {
		this.statusImageURL = statusImageURL;
	}
	public static void main(String[] args) {
		Presence presence = new Presence(Presence.Type.unavailable);
		System.out.println(presence);
		System.out.println(presence.getPriority());
		Show[] shows = Show.values();
		for (int i = 0; i < shows.length; i++) {
			System.out.println(shows[i]);
		}
	}
}
