package com.kingray.openfire.plugin.vo;

import java.util.Collection;

public class PageVO implements VO{
	private int pageNumber;// 分页编号
	private int pageSize; // 每个分页的记录书
	private int pageCount; // 分页总数
	private int recordCount; // 记录总数
	private Collection pageContents;
	
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	/**
	 * 页面内容
	 */
	public Collection getPageContents() {
		return pageContents;
	}
	public void setPageContents(Collection pageContents) {
		this.pageContents = pageContents;
	}
	@Override
	public String toString() {
		return this.getClass() + "@" + hashCode() + "[ pageNumber: " + pageNumber 
				+ ", pageSize: " + pageSize + ", pageCount: " 
				+ pageCount + ", recordCount: " + recordCount
				+ ", pageContents: " + pageContents.toString() + " ]";
	}
	
}
