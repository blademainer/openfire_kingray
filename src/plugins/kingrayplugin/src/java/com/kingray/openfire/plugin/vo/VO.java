package com.kingray.openfire.plugin.vo;

import java.io.Serializable;
/**
 * VO类，所有实体类的超类
 * @author KRXiongYingqi
 * @version 2013-6-20 下午4:02:49
 */
public interface VO extends Serializable{

}
