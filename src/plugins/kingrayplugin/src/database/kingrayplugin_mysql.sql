/*==============================================================*/
/* Table: kr_largest_message_history                            */
/*==============================================================*/
create table kr_emotion
(
	emotion_id int not null auto_increment,
	emotion_uuid varchar(255),
	emotion_string varchar(128),
	emotion_file_path varchar(255),
	primary key (emotion_id)
);

/*==============================================================*/
/* Index: emotion_id                                    		*/
/*==============================================================*/
create index emotion_id_index on kr_emotion
(
   emotion_id
);

/*==============================================================*/
/* Table: kr_largest_message_history                            */
/*==============================================================*/
create table kr_largest_message_history
(
   largest_message_id   int not null auto_increment,
   largest_message_body mediumtext,
   primary key (largest_message_id)
);

/*==============================================================*/
/* Table: kr_long_message_history                               */
/*==============================================================*/
create table kr_long_message_history
(
   long_message_id      int not null auto_increment,
   long_message_body    text,
   primary key (long_message_id)
);

/*==============================================================*/
/* Table: kr_message_history                                    */
/*==============================================================*/
create table kr_message_history
(
   message_id           int not null auto_increment,
   send_message_id		varchar(64),
   long_message_id      int default NULL,
   largest_message_id   int default NULL,
   message_from         varchar(64),
   message_to           varchar(64),
   message_subject      varchar(64),
   message_body         varchar(64),
   message_date_time    timestamp,
   message_type			varchar(64),
   primary key (message_id)
);

/*==============================================================*/
/* Index: send_message_id                                    */
/*==============================================================*/
create index send_message_id_index on kr_message_history
(
   send_message_id
);

/*==============================================================*/
/* Index: message_from_index                                    */
/*==============================================================*/
create index message_from_index on kr_message_history
(
   message_from
);

/*==============================================================*/
/* Index: message_to_index                                      */
/*==============================================================*/
create index message_to_index on kr_message_history
(
   message_to
);

/*==============================================================*/
/* Index: message_body_index                                    */
/*==============================================================*/
create index message_body_index on kr_message_history
(
   message_body
);

/*==============================================================*/
/* Index: message_datetime_index                                */
/*==============================================================*/
create index message_datetime_index on kr_message_history
(
   message_date_time
);

/*==============================================================*/
/* Index: message_subject_index                                 */
/*==============================================================*/
create index message_subject_index on kr_message_history
(
   message_subject
);

alter table kr_message_history add constraint FK_Reference_1 foreign key (long_message_id)
      references kr_long_message_history (long_message_id) on delete restrict on update restrict;

alter table kr_message_history add constraint FK_Reference_2 foreign key (largest_message_id)
      references kr_largest_message_history (largest_message_id) on delete restrict on update restrict;
      
INSERT INTO ofVersion (name, version) VALUES ('kingrayplugin', 4);