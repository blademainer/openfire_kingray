<%@ page language="java"  contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*,
                 org.jivesoftware.openfire.XMPPServer,
                 org.jivesoftware.util.*,com.kingray.openfire.plugin.KingrayServicePlugin"
    errorPage="error.jsp"
%>
<html>
	<head>
		<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
		<script	type="text/javascript">
		<!--
		
		$(function(){
		$(window).keydown(function(event){
			if(event.keycode == 8){
				event.returnvalue = false; // 取消浏览器事件
			}
		});
		/**
			var url = "http://10.188.199.170/plugins/kingrayplugin/userservice?actionType=getAllUsers";
			$.getJSON(url, function(callBack){
				var groups_div = $(".groups_div");
				var groups = jQuery.parseJSON(callBack);
				alert(groups);
				groups_div.html("");
				$.each(groups, function(i, group){
					alert(group.groupName);
					groups_div.append("group: " + group.groupName);
					$.each(group.userStatusVOs, function(i, userStatusVO){
						groups_div.append(", User: " + userStatusVO.userName);
					});
				});
			});
			*/
			/**
			*/
			var url = "/plugins/kingrayplugin/userservice?actionType=getAllUsers";
			$.post(url, function(callBack){
				$(".loading").remove();
				var groups_div = $(".groupsDiv"); // 分组树根
				var temp_group_div = $(groups_div).next(".groupDiv"); // 分组模型，用于拷贝
				
				var groups = jQuery.parseJSON(callBack);
				$.each(groups, function(i, group){
					var groupDiv = $(temp_group_div).clone(); // 拷贝分组
					
					$(groupDiv).children().children(".groupName").html(group.groupName); // 插入分组名
	
					var groupMembersDiv = groupDiv.children(".groupMembers");
					$.each(group.userStatusVOs, function(i, userStatusVO){
						groupMembersDiv.append("<div class='groupMember' id='" + userStatusVO.userName + "'></div>");
						var memberDiv = groupMembersDiv.children("#" + userStatusVO.userName);
						memberDiv.append("<span class='userName " + userStatusVO.show + "'>" + userStatusVO.userName + "</span><span class='statusSpan'><img src='" + userStatusVO.statusImageURL + "'></img><span class='statusName'>" + userStatusVO.status + "</span></span>");
					});
					$(groupDiv).show();
					groups_div.append(groupDiv); // 连接到分组树
				});
				// 点击分组时展开或收起分组
				$(".extensionDiv").toggle(
					function(){
						$(this).next(".groupMembers").stop();
						$(this).next(".groupMembers").slideDown();
						$(this).children(".unextensionImg").attr("src", "../images/extension.png");
					},
					function(){
						$(this).next(".groupMembers").stop();
						$(this).next(".groupMembers").slideUp();
						$(this).children(".unextensionImg").attr("src", "../images/unextension.png");
					}
				);
				
				$(".groupMember").live("click", function(){
					var userName = $(this).children(".userName").html();
					addMessageTo(userName);
				});
				/**
				* 添加收件人
				*/
				function addMessageTo(userName){
					var messageTos = $(".messageToDiv");
					/**
					var existsMessageTo= messageTos.children("#" + userName);
					alert(existsMessageTo.html() != "");
					if(existsMessageTo.html() != ""){
						messageTos.remove(existsMessageTo);
					}
					*/
					$(".messageTo").remove("#" + userName);
					var newMessageTo = $(messageTos).next(".messageTo").clone();
					
					$(newMessageTo).attr("id", userName);
					$(newMessageTo).children(".userNameSpan").html(userName);
					$(newMessageTo).show();
					messageTos.append(newMessageTo);
				}
				$(".messageTo").live("click", function(){
					var userName = $(this).children(".userNameSpan").html();
					$(this).live("keydown", function(event){
						//alert($(this).html());
						//$("#console").append($(this).html());
						//$("#console").append(event.keyCode);
						if(event.keyCode == 8){
							$(".messageTo").remove("#" + userName);
							return false;// 禁止回车键
						}
						//alert(event.keyCode);
						//alert($(".messageTo").focus().html());
					});
				});
			});
		});
		-->
		</script>
		<style type="text/css">
		<!--
		.alignRight{
			text-align: right;
		}
		.loading_image_div{
		}
		.loading_image{
			float: left;
		}
		.loading_label{
			float: left;
		}
		.extensionDiv{
			cursor: hand;
		}
		.unextensionImg {
			opacity: 0.15;
		}
		.groupsDiv{
			position: absolute;
			width: 200px;
		}
		.groupMembers {
			padding-left: 20px;
		}
		.groupMember{
			cursor: hand;
		}
		.chatDiv{
			position: absolute;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			left: 300px;
			top: 50px;
			border-color: #08c;
			width: 550px;
		}
		.messageToDiv {
			margin-right: auto;
			padding: 3px;
			outline: 0;
			border: 1px solid gray;
			word-wrap: break-word;
			overflow-x: hidden;
			overflow-y: auto;
			overflow-y: visible;
		}
		.messageTo{
			float: left;
			cursor: hand;
			height: 35px;
			*height: 30px;
			padding: 3px;
		}
		.messageTo:hover{
			background-color: rgb(0, 156, 253);
		}
		.messageTo:focus{
			background-color: #08c;
		}
		
		
		.divText{
			border: 2px #3E97D1 solid;
			width: 100%;
			background: white;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			overflow: hidden;
			cursor: text;
			height: 23px;
			*height: 30px;
		}
		.text{
			border: 2px #3E97D1 solid;
			background: white;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			overflow: hidden;
			cursor: text;
			height: 33px;
			*height: 30px;
		}
		.textArea{
			border: 2px #3E97D1 solid;
			background: white;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			overflow: hidden;
			cursor: text;
		}
		-->
		</style>
	</head>
	<body>
		<div class="loading"><div class="loading_image_div"><img src="../images/loading.gif" class="loading_image"></img></div></div>
		<!-- 分组 -->
		<div class="groupsDiv">
			
		</div>
		<!-- 单个分组模型 -->
		<div class="groupDiv" style="display:none;">
			<div class="extensionDiv unextension">
				<img src="../images/unextension.png" color=gray class="unextensionImg"></img>
				<span class="groupName"></span>
			</div>
			<div class="groupMembers" style="display:none;">
			</div>
		</div>
		
		<!-- 聊天窗口  style="display:none;" -->
		<div class="chatDiv">
			<form action="" method="post">
				<fieldset>
					<legend>发送消息</legend>
					<table style="width:100%;">
						<tr>
							<td style="width: 80px;" class="alignRight">
								收件人：
							</td>
							<td>
								<div contenteditable="false"  class="messageToDiv divText"></div>
								<span class="messageTo" style="display: none;" tabindex="0"><span class="userNameSpan"></span><span>; </span></span>
							</td>
						</tr>
						<tr>
							<td style="width: 80px;" class="alignRight">
								消息内容：
							</td>
							<td>
								<textarea rows="10" cols="62" class="textArea"></textarea>
							</td>
						</tr>
					</table>
				</fieldset>
			</form>
		</div>
	</body>
</html>