<%@ page language="java" contentType="text/html; charset=UTF-8"  %>
<%@ page import="java.util.*,
                 org.jivesoftware.openfire.XMPPServer,
                 org.jivesoftware.util.*,com.kingray.openfire.plugin.KingrayServicePlugin"
%>
<html>
	<head>
		<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
		<style type="text/css">
		<!--
		.alignRight{
			text-align: right;
		}
		.loading_image_div{
		}
		.loading_image{
			float: left;
		}
		.loading_label{
			float: left;
		}
		.extensionDiv{
			cursor: hand;
		}
		.unextensionImg {
			opacity: 0.15;
		}
		.groupsDiv{
			position: absolute;
			width: 200px;
		}
		.groupMembers {
			padding-left: 20px;
		}
		.groupMember{
			cursor: hand;
		}
		.chatDiv{
			position: absolute;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			left: 300px;
			top: 50px;
			border-color: #08c;
			width: 550px;
		}
		.messageToDiv {
			margin-right: auto;
			padding: 3px;
			outline: 0;
			border: 1px solid gray;
			word-wrap: break-word;
			overflow-x: hidden;
			overflow-y: auto;
			overflow-y: visible;
		}
		.messageTo{
			float: left;
			cursor: hand;
			height: 35px;
			*height: 30px;
			padding: 3px;
		}
		.messageTo:hover{
			background-color: rgb(0, 156, 253);
		}
		.messageTo:focus{
			background-color: #08c;
		}
		.divText{
			border: 2px #3E97D1 solid;
			width: 100%;
			background: white;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			overflow: hidden;
			cursor: text;
			height: 23px;
			*height: 30px;
		}
		.text{
			border: 2px #3E97D1 solid;
			background: white;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			overflow: hidden;
			cursor: text;
			height: 33px;
			*height: 30px;
		}
		.textArea{
			border: 2px #3E97D1 solid;
			background: white;
			box-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);
			border-radius: 3px;
			overflow: hidden;
			cursor: text;
		}
		#imgDiv{
			width: 15px;
			height: 15px;
		}
		#notificationImage{
			width: 15px;
			height: 15px;
			background-position: -72px 0;
			background-image: url("../images/glyphicons-halflings.png");
		}
		#notificationDisplay{
			vertical-align:sub;
			color: red; 
			font-size:5px; 
			font-family:Arial, Helvetica, sans-serif;
			margin:0px 0px 0px 0px
		}
		#notificationDiv{
			width: 30px;
			height: 20px;
		}
		.notifyDiv{
			float: left;
		}
		-->
		</style>
		<script	type="text/javascript">
		<!--
		var userName = "<%=request.getSession().getAttribute("userName")%>";
		var messageCount = 0;
		/**
		* 轮询
		*/
		function circulateGetMessageCount(){
			var tempMessageCount = getMessageCount(userName);
			//tempMessageCount = 1;
			$("#notificationDisplay").html(tempMessageCount);
			if(tempMessageCount > 0){
				if(messageCount <= 0){
					startNotify();
				}
			} else {
				if(messageCount >= 0){
					stopNotify();
				}
			}
			messageCount = tempMessageCount;
			setTimeout("circulateGetMessageCount()", 3000);

		}
		
		var getMessageCountTemp = 0;
		function getMessageCount(userName){
			var url = "/plugins/kingrayplugin/messageservice?actionType=getOfflineMessageCount&userName=" + userName;
			//var url = "http://127.0.0.1:9090/plugins/kingrayplugin/messageservice?actionType=getOfflineMessageCount&userName=" + userName;
			$.post(url, function(callBack){
				getMessageCountTemp = callBack;
			});
			return getMessageCountTemp;
		}
		
		var notifyRef = null;
		/*
		* 开始通知
		*/
		function startNotify(){
			//notifyRef = setTimeout("startNotify()", 500);
			notify();
			notifyRef = setTimeout('startNotify()', 500);
			
		}
		
		function notify(){
			var notificationImage = $("#notificationDiv");
			if($(notificationImage).css("display") == "none"){
				$(notificationImage).show();
			} else {
				$(notificationImage).hide();
			}
			//startNotify();
			
		}
		function stopNotify(){
			if(notifyRef != null){
				clearTimeout(notifyRef);
			}
		}
		$(function(){
	
			$(window).ready(function(){
				circulateGetMessageCount();
			});
			
			
			//clearTimeout(flag1);
		});
		-->
		</script>
		
	</head>
	<body>
		<a href="/plugins/kingrayplugin/messageservice?actionType=showOfflineMessagePage&userName=<%=request.getSession().getAttribute("userName")%>" target="offLineMessage">
			<div id="notificationDiv">
				<div id="imgDiv" class="notifyDiv">
					<div id="notificationImage"></div>
				</div>
				<div id="notificationDisplay" class="notifyDiv"></div>
			</div>
		</a>

	</body>
</html>