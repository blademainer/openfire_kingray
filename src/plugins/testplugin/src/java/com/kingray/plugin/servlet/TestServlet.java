package com.kingray.plugin.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jivesoftware.admin.AuthCheckFilter;
import org.jivesoftware.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestServlet extends HttpServlet{
	Logger log = LoggerFactory.getLogger(TestServlet.class);
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		AuthCheckFilter.addExclude("testplugin/testServlet");
	}
	@Override
	public void destroy() {
		super.destroy();
		AuthCheckFilter.removeExclude("testplugin/testServlet");
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		log.debug("actionType ========== " + request.getParameter("actionType"));
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}
}
