package com.kingray.plugin;

import java.io.File;
import java.util.Map;

import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.util.PropertyEventListener;

public class TestPlugin implements Plugin, PropertyEventListener{

	@Override
	public void initializePlugin(PluginManager manager, File pluginDirectory) {
		
	}

	@Override
	public void destroyPlugin() {
		
	}

	@Override
	public void propertySet(String property, Map<String, Object> params) {
		
	}

	@Override
	public void propertyDeleted(String property, Map<String, Object> params) {
		
	}

	@Override
	public void xmlPropertySet(String property, Map<String, Object> params) {
		
	}

	@Override
	public void xmlPropertyDeleted(String property, Map<String, Object> params) {
		
	}

}
