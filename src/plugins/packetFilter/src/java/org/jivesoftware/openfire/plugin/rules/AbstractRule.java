package org.jivesoftware.openfire.plugin.rules;

import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.xmpp.packet.Packet;


public abstract class AbstractRule implements Rule {

    private Action packetAction;
    private PacketType packetType;
    private Boolean disabled;
    private String source;
    private String destination;
    private Boolean log;
    private String description;
    private String ruleId;
    private String displayName;
    private SourceDestType sourceType;
    private SourceDestType destType;


    @Override
	public SourceDestType getDestType() {
        return destType;
    }

    @Override
	public void setDestType(SourceDestType destType) {
        this.destType = destType;
    }

    @Override
	public SourceDestType getSourceType() {
        return sourceType;
    }

    @Override
	public void setSourceType(SourceDestType sourceType) {
        this.sourceType = sourceType;
    }

    @Override
	public int getOrder() {
        return order;
    }

    @Override
	public void setOrder(int order) {
        this.order = order;
    }

    private int order;


    @Override
	public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
	public String getDisplayName() {
        return displayName;
    }

    @Override
	public String getRuleId() {
        return ruleId;
    }

    @Override
	public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    @Override
	public String getRuleType() {
        return this.getClass().getName();
    }


    @Override
	public Action getPacketAction() {
        return packetAction;
    }

    @Override
	public void setPacketAction(Action packetAction) {
        this.packetAction = packetAction;
    }

    @Override
	public PacketType getPackeType() {
        return packetType;
    }

    @Override
	public void setPacketType(PacketType packetType) {
        this.packetType = packetType;
    }

    @Override
	public Boolean isDisabled() {
        return disabled;
    }

    @Override
	public void isDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    @Override
	public String getSource() {
        return source;
    }

    @Override
	public void setSource(String source) {
        this.source = source;
    }

    @Override
	public String getDestination() {
        return destination;
    }

    @Override
	public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
	public Boolean doLog() {
        return log;
    }

    @Override
	public void doLog(Boolean log) {
        this.log = log;
    }

    @Override
	public String getDescription() {
        return description;
    }

    @Override
	public void setDescription(String description) {
        this.description = description;
    }

    @Override
	public Packet doAction(Packet packet) throws PacketRejectedException {
        return null;
    }

    public boolean sourceMustMatch() {
        return true;
    }

    public boolean destMustMatch() {
        return true;
    }


    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        if (packetAction != null)
        sb.append("Type : "+packetAction.toString()+'\n');
        sb.append("Order : "+getOrder()+'\n');
        sb.append("Diplay Name : "+getDisplayName()+'\n');
        sb.append("Packet Type : "+packetType+'\n');
        sb.append("ID : "+ruleId+'\n');
        sb.append("Soruce Type : "+sourceType+'\n');
        sb.append("Source : "+source+'\n');
        sb.append("Dest Type : "+destType+'\n');
        sb.append("Destination : "+destination+'\n');
        sb.append("Loging : "+log+'\n');
        sb.append("Disabled : "+disabled+'\n');
        return sb.toString();
    }
}
