package org.jivesoftware.openfire.plugin.rules;


import java.util.List;

public class RuleManagerProxy implements RuleManager {

    private DbRuleManager dbRuleManager = DbRuleManager.getInstance();

    public RuleManagerProxy() {
    }

    @Override
	public Rule getRuleById(int id) {
        //Pull it from the db
        return dbRuleManager.getRuleById(id);
    }

    @Override
	public List<Rule> getRules() {
        return  dbRuleManager.getRules();
    }

    @Override
	public void addRule(Rule rule, Integer order) {
         
    }

    @Override
	public void addRule(Rule rule) {
        dbRuleManager.addRule(rule);
    }

    @Override
	public void deleteRule(int ruleId) {
        //Remove rule from storage (db)
        dbRuleManager.deleteRule(ruleId);
    }

    @Override
	public void moveOne(int srcId, int destId) {
        Rule srcRule = dbRuleManager.getRuleById(srcId);
        Rule destRule = dbRuleManager.getRuleById(destId);

        dbRuleManager.moveOne(srcRule,destRule);
        //rulesUpdated();

    }

    @Override
	public int getLastOrder() {
        return dbRuleManager.getLastOrderId();
    }

    @Override
	public void moveRuleOrder(int ruleId,int orderId) {
        dbRuleManager.moveRuleOrder(ruleId,orderId);
       // rulesUpdated();
    }

    @Override
	public void updateRule(Rule rule) {
       dbRuleManager.updateRule(rule);
       //rulesUpdated();
    }

    @Override
	public void rulesUpdated() {
        reloadRules();
    }

    private void reloadRules() {
        dbRuleManager.clear();
    }
}
