package org.jivesoftware.openfire.plugin.kingrayplugin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.util.*;
import com.kingray.openfire.plugin.KingrayServicePlugin;

public final class kingray_002dplugin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var_value_nobody;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_set_var_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_set_var_value_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      org.jivesoftware.util.WebManager admin = null;
      synchronized (_jspx_page_context) {
        admin = (org.jivesoftware.util.WebManager) _jspx_page_context.getAttribute("admin", PageContext.PAGE_SCOPE);
        if (admin == null){
          admin = new org.jivesoftware.util.WebManager();
          _jspx_page_context.setAttribute("admin", admin, PageContext.PAGE_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_set_0(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
 admin.init(request, response, session, application, out ); 

      out.write("\r\n\r\n");
  // Get parameters
    boolean save = request.getParameter("save") != null;
    boolean success = request.getParameter("success") != null;
    String secret = ParamUtils.getParameter(request, "secret");
    boolean enabled = ParamUtils.getBooleanParameter(request, "enabled");
    String allowedIPs = ParamUtils.getParameter(request, "allowedIPs");

    KingrayServicePlugin plugin = (KingrayServicePlugin) XMPPServer.getInstance().getPluginManager().getPlugin("kingrayplugin");

    // Handle a save
    Map errors = new HashMap();
    if (save) {
        if (errors.size() == 0) {
            plugin.setEnabled(enabled);
        	plugin.setSecret(secret);
            plugin.setAllowedIPs(StringUtils.stringToCollection(allowedIPs));
            response.sendRedirect("kingray-plugin.jsp?success=true");
            return;
        }
    }

    secret = plugin.getSecret();
    enabled = plugin.isEnabled();
    allowedIPs = StringUtils.collectionToString(plugin.getAllowedIPs());

      out.write("\r\n\r\n<html>\r\n    <head>\r\n        <title>用户服务属性</title>\r\n        <meta name=\"pageID\" content=\"kingray-plugin\"/>\r\n    </head>\r\n    <body>\r\n\r\n\r\n<p>\r\n使用下面的表格来启用或禁用用户服务和配置密钥。\r\n此服务默认是 <strong>关闭</strong>, 所以该服务默认是无效的。\r\n</p>\r\n\r\n");
  if (success) { 
      out.write("\r\n\r\n    <div class=\"jive-success\">\r\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n        \t<td class=\"jive-icon\"><img src=\"images/success-16x16.gif\" width=\"16\" height=\"16\" border=\"0\"></td>\r\n        \t<td class=\"jive-icon-label\">\r\n            \t用户服务属性编辑成功！\r\n        \t</td>\r\n        </tr>\r\n    </tbody>\r\n    </table>\r\n    </div><br>\r\n");
 } 
      out.write("\r\n\r\n<form action=\"kingray-plugin.jsp?save\" method=\"post\">\r\n\r\n<fieldset>\r\n    <legend>用户服务</legend>\r\n    <div>\r\n    <p>\r\n    \t用户的添加、删除和编辑的通常在管理控制台的外部是不可用的。这项服务可以提供给外部应用使用http请求来控制用户信息。\r\n\t</p>\r\n\r\n    <p>\r\n    \t然而,这种服务存在一个安全风险。因此，请求该服务必须提供一个有效的密钥。此外，你可以指定允许访问该服务的IP地址列表。一个空的列表意味着任何地址都能访问该服务。多个地址使用逗号进行分隔。\r\n    </p>\r\n    <ul>\r\n        <input type=\"radio\" name=\"enabled\" value=\"true\" id=\"rb01\"\r\n        ");
      out.print( ((enabled) ? "checked" : "") );
      out.write(">\r\n        <label for=\"rb01\"><b>启用</b> - 允许用户服务请求.</label>\r\n        <br>\r\n        <input type=\"radio\" name=\"enabled\" value=\"false\" id=\"rb02\"\r\n         ");
      out.print( ((!enabled) ? "checked" : "") );
      out.write(">\r\n        <label for=\"rb02\"><b>关闭</b> - 拒绝用户服务请求.</label>\r\n        <br><br>\r\n\r\n        <label for=\"text_secret\">密钥:</label>\r\n        <input type=\"text\" name=\"secret\" value=\"");
      out.print( secret );
      out.write("\" id=\"text_secret\">\r\n        <br><br>\r\n\r\n        <label for=\"text_secret\">允许的IP地址:</label>\r\n        <textarea name=\"allowedIPs\" cols=\"40\" rows=\"3\" wrap=\"virtual\">");
      out.print( ((allowedIPs != null) ? allowedIPs : "") );
      out.write("</textarea>\r\n    </ul>\r\n    </div>\r\n</fieldset>\r\n\r\n<br><br>\r\n\r\n<input type=\"submit\" value=\"保存修改\">\r\n</form>\r\n\r\n\r\n</body>\r\n</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_set_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_0.setPageContext(_jspx_page_context);
    _jspx_th_c_set_0.setParent(null);
    _jspx_th_c_set_0.setVar("admin");
    _jspx_th_c_set_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${admin.manager}", java.lang.Object.class, (PageContext)_jspx_page_context, null, false));
    int _jspx_eval_c_set_0 = _jspx_th_c_set_0.doStartTag();
    if (_jspx_th_c_set_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
    return false;
  }
}
