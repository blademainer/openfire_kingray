package org.jivesoftware.openfire.plugin.kingrayplugin.pages;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.util.*;
import com.kingray.openfire.plugin.KingrayServicePlugin;

public final class offline_005fmessage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n<html>\r\n\t<head>\r\n\t\t<script type=\"text/javascript\" src=\"../js/jquery-1.7.2.min.js\"></script>\r\n\t\t<script\ttype=\"text/javascript\">\r\n\t\t<!--\r\n\t\t$(function(){\r\n\t\t\tvar offLineMessages = jQuery.parseJSON('");
      out.print(request.getSession().getAttribute("jsonOfflineMessages"));
      out.write("');\r\n\t\t\tvar messageTable = $(\"#messageTable\");\r\n\t\t\t$(window).ready(function(){\r\n\t\t\t\t$.each(offLineMessages, function(i, offLineMessage){\r\n\t\t\t\t\tvar tr = \"<tr class='offlineMessage'  id='\" + offLineMessage.messageDateTime.time + \"'>\";\r\n\t\t\t\t\ttr += \"<td class='line-bottom-border messageTd'>\";\r\n\t\t\t\t\ttr += offLineMessage.messageFrom;\r\n\t\t\t\t\ttr += \"</td>\";\r\n\t\t\t\t\ttr += \"<td class='line-bottom-border messageTd'>\";\r\n\t\t\t\t\ttr += offLineMessage.messageDateTimeStr;\r\n\t\t\t\t\ttr += \"</td>\";\r\n\t\t\t\t\ttr += \"<td class='line-bottom-border messageTd'>\";\r\n\t\t\t\t\ttr += offLineMessage.messageSubject;\r\n\t\t\t\t\ttr += \"</td>\";\r\n\t\t\t\t\ttr += \"<td class='line-bottom-border messageTd'>\";\r\n\t\t\t\t\ttr += offLineMessage.messageBody;\r\n\t\t\t\t\ttr += \"</td>\";\r\n\t\t\t\t\ttr += \"</tr>\";\r\n\t\t\t\t\tmessageTable.append(tr);\r\n\t\t\t\t});\r\n\t\t\t});\r\n\t\t\t$(\".messageTd\").live(\"mouseover\", function(){\r\n\t\t\t\t$(this).parent(\".offlineMessage\").children(\"td\").removeClass(\"messageTd\");\r\n\t\t\t\t$(this).parent(\".offlineMessage\").children(\"td\").addClass(\"mouseOver\");\r\n\t\t\t\t\r\n\t\t\t});\r\n\t\t\t$(\".mouseOver\").live(\"mouseleave\", function(){\r\n");
      out.write("\t\t\t\t$(this).parent(\".offlineMessage\").children(\"td\").removeClass(\"mouseOver\");\r\n\t\t\t\t$(this).parent(\".offlineMessage\").children(\"td\").addClass(\"messageTd\");\r\n\t\t\t});\r\n\t\t});\r\n\t\t-->\r\n\t\t</script>\r\n\t\t<style type=\"text/css\">\r\n\t\t<!--\r\n\t\t#offlineMessageDiv{\r\n\t\t\tleft: 100px;\r\n\t\t}\r\n\t\ttable#messageTable tr td{\r\n\t\t\ttext-align: left;\r\n\t\t}\r\n\t\t.table-header {\r\n\t\t\ttext-align: left;\r\n\t\t\tfont-family: verdana, arial, helvetica, sans-serif;\r\n\t\t\tfont-size: 8pt;\r\n\t\t\tfont-weight: bold;\r\n\t\t\tborder-color: #ccc;\r\n\t\t\tborder-style: solid;\r\n\t\t\tborder-width: 1px 0 1px 0;\r\n\t\t\tpadding: 5px;\r\n\t\t}\r\n\t\ttr {\r\n\t\t\tdisplay: table-row;\r\n\t\t\tvertical-align: inherit;\r\n\t\t\tborder-color: inherit;\r\n\t\t}\r\n\t\t.mouseOver{\r\n\t\t\tbackground-color: rgb(0, 156, 253);\r\n\t\t}\r\n\t\t.line-bottom-border {\r\n\t\t\ttext-align: left;\r\n\t\t\tfont-family: verdana, arial, helvetica, sans-serif;\r\n\t\t\tfont-size: 9pt;\r\n\t\t\tborder-color: #e3e3e3;\r\n\t\t\tborder-style: solid;\r\n\t\t\tborder-width: 0px 0px 1px 0px;\r\n\t\t\tpadding: 5px;\r\n\t\t}\r\n\t\t.light-gray-border {\r\n\t\t\tborder-color: #ccc;\r\n\t\t\tborder-style: solid;\r\n");
      out.write("\t\t\tborder-width: 1px 1px 1px 1px;\r\n\t\t\tpadding: 5px;\r\n\t\t\t-moz-border-radius: 3px;\r\n\t\t}\r\n\t\ttable{\r\n\t\t\tborder-spacing: 0px;\r\n\t\t\tborder-top-width: 0px;\r\n\t\t\tborder-right-width: 0px;\r\n\t\t\tborder-bottom-width: 0px;\r\n\t\t\tborder-left-width: 0px;\r\n\t\t\twidth: 100%;\r\n\t\t}\r\n\t\t-->\r\n\t\t</style>\r\n\t</head>\r\n\t<body>\r\n\t\t<div id=\"offlineMessageDiv\" class=\"light-gray-border\">\r\n\t\t\t<table id=\"messageTable\">\r\n\t\t\t\t<tr style=\"background:#eee;\">\r\n\t\t\t\t\t<td width=\"10%;\" class=\"table-header\">\r\n\t\t\t\t\t发送人\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td width=\"20%;\" class=\"table-header\">\r\n\t\t\t\t\t发送时间\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td width=\"20%;\" class=\"table-header\">\r\n\t\t\t\t\t标题\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td width=\"50%;\" class=\"table-header\">\r\n\t\t\t\t\t内容\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr>\r\n\t\t</div>\r\n\t</body>\r\n</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
