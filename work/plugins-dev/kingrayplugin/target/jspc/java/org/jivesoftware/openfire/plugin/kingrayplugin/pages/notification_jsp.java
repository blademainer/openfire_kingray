package org.jivesoftware.openfire.plugin.kingrayplugin.pages;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.util.*;
import com.kingray.openfire.plugin.KingrayServicePlugin;

public final class notification_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n<html>\r\n\t<head>\r\n\t\t<script type=\"text/javascript\" src=\"../js/jquery-1.7.2.min.js\"></script>\r\n\t\t<style type=\"text/css\">\r\n\t\t<!--\r\n\t\t.alignRight{\r\n\t\t\ttext-align: right;\r\n\t\t}\r\n\t\t.loading_image_div{\r\n\t\t}\r\n\t\t.loading_image{\r\n\t\t\tfloat: left;\r\n\t\t}\r\n\t\t.loading_label{\r\n\t\t\tfloat: left;\r\n\t\t}\r\n\t\t.extensionDiv{\r\n\t\t\tcursor: hand;\r\n\t\t}\r\n\t\t.unextensionImg {\r\n\t\t\topacity: 0.15;\r\n\t\t}\r\n\t\t.groupsDiv{\r\n\t\t\tposition: absolute;\r\n\t\t\twidth: 200px;\r\n\t\t}\r\n\t\t.groupMembers {\r\n\t\t\tpadding-left: 20px;\r\n\t\t}\r\n\t\t.groupMember{\r\n\t\t\tcursor: hand;\r\n\t\t}\r\n\t\t.chatDiv{\r\n\t\t\tposition: absolute;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n\t\t\tborder-radius: 3px;\r\n\t\t\tleft: 300px;\r\n\t\t\ttop: 50px;\r\n\t\t\tborder-color: #08c;\r\n\t\t\twidth: 550px;\r\n\t\t}\r\n\t\t.messageToDiv {\r\n\t\t\tmargin-right: auto;\r\n\t\t\tpadding: 3px;\r\n\t\t\toutline: 0;\r\n\t\t\tborder: 1px solid gray;\r\n\t\t\tword-wrap: break-word;\r\n\t\t\toverflow-x: hidden;\r\n\t\t\toverflow-y: auto;\r\n\t\t\toverflow-y: visible;\r\n\t\t}\r\n\t\t.messageTo{\r\n\t\t\tfloat: left;\r\n\t\t\tcursor: hand;\r\n\t\t\theight: 35px;\r\n\t\t\t*height: 30px;\r\n\t\t\tpadding: 3px;\r\n");
      out.write("\t\t}\r\n\t\t.messageTo:hover{\r\n\t\t\tbackground-color: rgb(0, 156, 253);\r\n\t\t}\r\n\t\t.messageTo:focus{\r\n\t\t\tbackground-color: #08c;\r\n\t\t}\r\n\t\t.divText{\r\n\t\t\tborder: 2px #3E97D1 solid;\r\n\t\t\twidth: 100%;\r\n\t\t\tbackground: white;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n\t\t\tborder-radius: 3px;\r\n\t\t\toverflow: hidden;\r\n\t\t\tcursor: text;\r\n\t\t\theight: 23px;\r\n\t\t\t*height: 30px;\r\n\t\t}\r\n\t\t.text{\r\n\t\t\tborder: 2px #3E97D1 solid;\r\n\t\t\tbackground: white;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n\t\t\tborder-radius: 3px;\r\n\t\t\toverflow: hidden;\r\n\t\t\tcursor: text;\r\n\t\t\theight: 33px;\r\n\t\t\t*height: 30px;\r\n\t\t}\r\n\t\t.textArea{\r\n\t\t\tborder: 2px #3E97D1 solid;\r\n\t\t\tbackground: white;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n\t\t\tborder-radius: 3px;\r\n\t\t\toverflow: hidden;\r\n\t\t\tcursor: text;\r\n\t\t}\r\n\t\t#imgDiv{\r\n\t\t\twidth: 15px;\r\n\t\t\theight: 15px;\r\n\t\t}\r\n\t\t#notificationImage{\r\n\t\t\twidth: 15px;\r\n\t\t\theight: 15px;\r\n\t\t\tbackground-position: -72px 0;\r\n\t\t\tbackground-image: url(\"../images/glyphicons-halflings.png\");\r\n\t\t}\r\n\t\t#notificationDisplay{\r\n\t\t\tvertical-align:sub;\r\n");
      out.write("\t\t\tcolor: red; \r\n\t\t\tfont-size:5px; \r\n\t\t\tfont-family:Arial, Helvetica, sans-serif;\r\n\t\t\tmargin:0px 0px 0px 0px\r\n\t\t}\r\n\t\t#notificationDiv{\r\n\t\t\twidth: 30px;\r\n\t\t\theight: 20px;\r\n\t\t}\r\n\t\t.notifyDiv{\r\n\t\t\tfloat: left;\r\n\t\t}\r\n\t\t-->\r\n\t\t</style>\r\n\t\t<script\ttype=\"text/javascript\">\r\n\t\t<!--\r\n\t\tvar userName = \"");
      out.print(request.getSession().getAttribute("userName"));
      out.write("\";\r\n\t\tvar messageCount = 0;\r\n\t\t/**\r\n\t\t* 轮询\r\n\t\t*/\r\n\t\tfunction circulateGetMessageCount(){\r\n\t\t\tvar tempMessageCount = getMessageCount(userName);\r\n\t\t\t//tempMessageCount = 1;\r\n\t\t\t$(\"#notificationDisplay\").html(tempMessageCount);\r\n\t\t\tif(tempMessageCount > 0){\r\n\t\t\t\tif(messageCount <= 0){\r\n\t\t\t\t\tstartNotify();\r\n\t\t\t\t}\r\n\t\t\t} else {\r\n\t\t\t\tif(messageCount >= 0){\r\n\t\t\t\t\tstopNotify();\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t\tmessageCount = tempMessageCount;\r\n\t\t\tsetTimeout(\"circulateGetMessageCount()\", 3000);\r\n\r\n\t\t}\r\n\t\t\r\n\t\tvar getMessageCountTemp = 0;\r\n\t\tfunction getMessageCount(userName){\r\n\t\t\tvar url = \"/plugins/kingrayplugin/messageservice?actionType=getOfflineMessageCount&userName=\" + userName;\r\n\t\t\t//var url = \"http://127.0.0.1:9090/plugins/kingrayplugin/messageservice?actionType=getOfflineMessageCount&userName=\" + userName;\r\n\t\t\t$.post(url, function(callBack){\r\n\t\t\t\tgetMessageCountTemp = callBack;\r\n\t\t\t});\r\n\t\t\treturn getMessageCountTemp;\r\n\t\t}\r\n\t\t\r\n\t\tvar notifyRef = null;\r\n\t\t/*\r\n\t\t* 开始通知\r\n\t\t*/\r\n\t\tfunction startNotify(){\r\n\t\t\t//notifyRef = setTimeout(\"startNotify()\", 500);\r\n");
      out.write("\t\t\tnotify();\r\n\t\t\tnotifyRef = setTimeout('startNotify()', 500);\r\n\t\t\t\r\n\t\t}\r\n\t\t\r\n\t\tfunction notify(){\r\n\t\t\tvar notificationImage = $(\"#notificationDiv\");\r\n\t\t\tif($(notificationImage).css(\"display\") == \"none\"){\r\n\t\t\t\t$(notificationImage).show();\r\n\t\t\t} else {\r\n\t\t\t\t$(notificationImage).hide();\r\n\t\t\t}\r\n\t\t\t//startNotify();\r\n\t\t\t\r\n\t\t}\r\n\t\tfunction stopNotify(){\r\n\t\t\tif(notifyRef != null){\r\n\t\t\t\tclearTimeout(notifyRef);\r\n\t\t\t}\r\n\t\t}\r\n\t\t$(function(){\r\n\t\r\n\t\t\t$(window).ready(function(){\r\n\t\t\t\tcirculateGetMessageCount();\r\n\t\t\t});\r\n\t\t\t\r\n\t\t\t\r\n\t\t\t//clearTimeout(flag1);\r\n\t\t});\r\n\t\t-->\r\n\t\t</script>\r\n\t\t\r\n\t</head>\r\n\t<body>\r\n\t\t<a href=\"/plugins/kingrayplugin/messageservice?actionType=showOfflineMessagePage&userName=");
      out.print(request.getSession().getAttribute("userName"));
      out.write("\" target=\"offLineMessage\">\r\n\t\t\t<div id=\"notificationDiv\">\r\n\t\t\t\t<div id=\"imgDiv\" class=\"notifyDiv\">\r\n\t\t\t\t\t<div id=\"notificationImage\"></div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div id=\"notificationDisplay\" class=\"notifyDiv\"></div>\r\n\t\t\t</div>\r\n\t\t</a>\r\n\r\n\t</body>\r\n</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
