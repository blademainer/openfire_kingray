package org.jivesoftware.openfire.plugin.kingrayplugin.pages;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.util.*;
import com.kingray.openfire.plugin.KingrayServicePlugin;

public final class user_005flist_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n<html>\r\n\t<head>\r\n\t\t<script type=\"text/javascript\" src=\"../js/jquery-1.7.2.min.js\"></script>\r\n\t\t<script\ttype=\"text/javascript\">\r\n\t\t<!--\r\n\t\t\r\n\t\t$(function(){\r\n\t\t$(window).keydown(function(event){\r\n\t\t\tif(event.keycode == 8){\r\n\t\t\t\tevent.returnvalue = false; // 取消浏览器事件\r\n\t\t\t}\r\n\t\t});\r\n\t\t/**\r\n\t\t\tvar url = \"http://10.188.199.170/plugins/kingrayplugin/userservice?actionType=getAllUsers\";\r\n\t\t\t$.getJSON(url, function(callBack){\r\n\t\t\t\tvar groups_div = $(\".groups_div\");\r\n\t\t\t\tvar groups = jQuery.parseJSON(callBack);\r\n\t\t\t\talert(groups);\r\n\t\t\t\tgroups_div.html(\"\");\r\n\t\t\t\t$.each(groups, function(i, group){\r\n\t\t\t\t\talert(group.groupName);\r\n\t\t\t\t\tgroups_div.append(\"group: \" + group.groupName);\r\n\t\t\t\t\t$.each(group.userStatusVOs, function(i, userStatusVO){\r\n\t\t\t\t\t\tgroups_div.append(\", User: \" + userStatusVO.userName);\r\n\t\t\t\t\t});\r\n\t\t\t\t});\r\n\t\t\t});\r\n\t\t\t*/\r\n\t\t\t/**\r\n\t\t\t*/\r\n\t\t\tvar url = \"/plugins/kingrayplugin/userservice?actionType=getAllUsers\";\r\n\t\t\t$.post(url, function(callBack){\r\n\t\t\t\t$(\".loading\").remove();\r\n\t\t\t\tvar groups_div = $(\".groupsDiv\"); // 分组树根\r\n");
      out.write("\t\t\t\tvar temp_group_div = $(groups_div).next(\".groupDiv\"); // 分组模型，用于拷贝\r\n\t\t\t\t\r\n\t\t\t\tvar groups = jQuery.parseJSON(callBack);\r\n\t\t\t\t$.each(groups, function(i, group){\r\n\t\t\t\t\tvar groupDiv = $(temp_group_div).clone(); // 拷贝分组\r\n\t\t\t\t\t\r\n\t\t\t\t\t$(groupDiv).children().children(\".groupName\").html(group.groupName); // 插入分组名\r\n\t\r\n\t\t\t\t\tvar groupMembersDiv = groupDiv.children(\".groupMembers\");\r\n\t\t\t\t\t$.each(group.userStatusVOs, function(i, userStatusVO){\r\n\t\t\t\t\t\tgroupMembersDiv.append(\"<div class='groupMember' id='\" + userStatusVO.userName + \"'></div>\");\r\n\t\t\t\t\t\tvar memberDiv = groupMembersDiv.children(\"#\" + userStatusVO.userName);\r\n\t\t\t\t\t\tmemberDiv.append(\"<span class='userName \" + userStatusVO.show + \"'>\" + userStatusVO.userName + \"</span><span class='statusSpan'><img src='\" + userStatusVO.statusImageURL + \"'></img><span class='statusName'>\" + userStatusVO.status + \"</span></span>\");\r\n\t\t\t\t\t});\r\n\t\t\t\t\t$(groupDiv).show();\r\n\t\t\t\t\tgroups_div.append(groupDiv); // 连接到分组树\r\n\t\t\t\t});\r\n\t\t\t\t// 点击分组时展开或收起分组\r\n\t\t\t\t$(\".extensionDiv\").toggle(\r\n\t\t\t\t\tfunction(){\r\n");
      out.write("\t\t\t\t\t\t$(this).next(\".groupMembers\").stop();\r\n\t\t\t\t\t\t$(this).next(\".groupMembers\").slideDown();\r\n\t\t\t\t\t\t$(this).children(\".unextensionImg\").attr(\"src\", \"../images/extension.png\");\r\n\t\t\t\t\t},\r\n\t\t\t\t\tfunction(){\r\n\t\t\t\t\t\t$(this).next(\".groupMembers\").stop();\r\n\t\t\t\t\t\t$(this).next(\".groupMembers\").slideUp();\r\n\t\t\t\t\t\t$(this).children(\".unextensionImg\").attr(\"src\", \"../images/unextension.png\");\r\n\t\t\t\t\t}\r\n\t\t\t\t);\r\n\t\t\t\t\r\n\t\t\t\t$(\".groupMember\").live(\"click\", function(){\r\n\t\t\t\t\tvar userName = $(this).children(\".userName\").html();\r\n\t\t\t\t\taddMessageTo(userName);\r\n\t\t\t\t});\r\n\t\t\t\t/**\r\n\t\t\t\t* 添加收件人\r\n\t\t\t\t*/\r\n\t\t\t\tfunction addMessageTo(userName){\r\n\t\t\t\t\tvar messageTos = $(\".messageToDiv\");\r\n\t\t\t\t\t/**\r\n\t\t\t\t\tvar existsMessageTo= messageTos.children(\"#\" + userName);\r\n\t\t\t\t\talert(existsMessageTo.html() != \"\");\r\n\t\t\t\t\tif(existsMessageTo.html() != \"\"){\r\n\t\t\t\t\t\tmessageTos.remove(existsMessageTo);\r\n\t\t\t\t\t}\r\n\t\t\t\t\t*/\r\n\t\t\t\t\t$(\".messageTo\").remove(\"#\" + userName);\r\n\t\t\t\t\tvar newMessageTo = $(messageTos).next(\".messageTo\").clone();\r\n\t\t\t\t\t\r\n\t\t\t\t\t$(newMessageTo).attr(\"id\", userName);\r\n");
      out.write("\t\t\t\t\t$(newMessageTo).children(\".userNameSpan\").html(userName);\r\n\t\t\t\t\t$(newMessageTo).show();\r\n\t\t\t\t\tmessageTos.append(newMessageTo);\r\n\t\t\t\t}\r\n\t\t\t\t$(\".messageTo\").live(\"click\", function(){\r\n\t\t\t\t\tvar userName = $(this).children(\".userNameSpan\").html();\r\n\t\t\t\t\t$(this).live(\"keydown\", function(event){\r\n\t\t\t\t\t\t//alert($(this).html());\r\n\t\t\t\t\t\t//$(\"#console\").append($(this).html());\r\n\t\t\t\t\t\t//$(\"#console\").append(event.keyCode);\r\n\t\t\t\t\t\tif(event.keyCode == 8){\r\n\t\t\t\t\t\t\t$(\".messageTo\").remove(\"#\" + userName);\r\n\t\t\t\t\t\t\treturn false;// 禁止回车键\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t\t//alert(event.keyCode);\r\n\t\t\t\t\t\t//alert($(\".messageTo\").focus().html());\r\n\t\t\t\t\t});\r\n\t\t\t\t});\r\n\t\t\t});\r\n\t\t});\r\n\t\t-->\r\n\t\t</script>\r\n\t\t<style type=\"text/css\">\r\n\t\t<!--\r\n\t\t.alignRight{\r\n\t\t\ttext-align: right;\r\n\t\t}\r\n\t\t.loading_image_div{\r\n\t\t}\r\n\t\t.loading_image{\r\n\t\t\tfloat: left;\r\n\t\t}\r\n\t\t.loading_label{\r\n\t\t\tfloat: left;\r\n\t\t}\r\n\t\t.extensionDiv{\r\n\t\t\tcursor: hand;\r\n\t\t}\r\n\t\t.unextensionImg {\r\n\t\t\topacity: 0.15;\r\n\t\t}\r\n\t\t.groupsDiv{\r\n\t\t\tposition: absolute;\r\n\t\t\twidth: 200px;\r\n\t\t}\r\n\t\t.groupMembers {\r\n");
      out.write("\t\t\tpadding-left: 20px;\r\n\t\t}\r\n\t\t.groupMember{\r\n\t\t\tcursor: hand;\r\n\t\t}\r\n\t\t.chatDiv{\r\n\t\t\tposition: absolute;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n\t\t\tborder-radius: 3px;\r\n\t\t\tleft: 300px;\r\n\t\t\ttop: 50px;\r\n\t\t\tborder-color: #08c;\r\n\t\t\twidth: 550px;\r\n\t\t}\r\n\t\t.messageToDiv {\r\n\t\t\tmargin-right: auto;\r\n\t\t\tpadding: 3px;\r\n\t\t\toutline: 0;\r\n\t\t\tborder: 1px solid gray;\r\n\t\t\tword-wrap: break-word;\r\n\t\t\toverflow-x: hidden;\r\n\t\t\toverflow-y: auto;\r\n\t\t\toverflow-y: visible;\r\n\t\t}\r\n\t\t.messageTo{\r\n\t\t\tfloat: left;\r\n\t\t\tcursor: hand;\r\n\t\t\theight: 35px;\r\n\t\t\t*height: 30px;\r\n\t\t\tpadding: 3px;\r\n\t\t}\r\n\t\t.messageTo:hover{\r\n\t\t\tbackground-color: rgb(0, 156, 253);\r\n\t\t}\r\n\t\t.messageTo:focus{\r\n\t\t\tbackground-color: #08c;\r\n\t\t}\r\n\t\t\r\n\t\t\r\n\t\t.divText{\r\n\t\t\tborder: 2px #3E97D1 solid;\r\n\t\t\twidth: 100%;\r\n\t\t\tbackground: white;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n\t\t\tborder-radius: 3px;\r\n\t\t\toverflow: hidden;\r\n\t\t\tcursor: text;\r\n\t\t\theight: 23px;\r\n\t\t\t*height: 30px;\r\n\t\t}\r\n\t\t.text{\r\n\t\t\tborder: 2px #3E97D1 solid;\r\n\t\t\tbackground: white;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n");
      out.write("\t\t\tborder-radius: 3px;\r\n\t\t\toverflow: hidden;\r\n\t\t\tcursor: text;\r\n\t\t\theight: 33px;\r\n\t\t\t*height: 30px;\r\n\t\t}\r\n\t\t.textArea{\r\n\t\t\tborder: 2px #3E97D1 solid;\r\n\t\t\tbackground: white;\r\n\t\t\tbox-shadow: inset 2px 2px 3px rgba(0, 0, 0, 0.2);\r\n\t\t\tborder-radius: 3px;\r\n\t\t\toverflow: hidden;\r\n\t\t\tcursor: text;\r\n\t\t}\r\n\t\t-->\r\n\t\t</style>\r\n\t</head>\r\n\t<body>\r\n\t\t<div class=\"loading\"><div class=\"loading_image_div\"><img src=\"../images/loading.gif\" class=\"loading_image\"></img></div></div>\r\n\t\t<!-- 分组 -->\r\n\t\t<div class=\"groupsDiv\">\r\n\t\t\t\r\n\t\t</div>\r\n\t\t<!-- 单个分组模型 -->\r\n\t\t<div class=\"groupDiv\" style=\"display:none;\">\r\n\t\t\t<div class=\"extensionDiv unextension\">\r\n\t\t\t\t<img src=\"../images/unextension.png\" color=gray class=\"unextensionImg\"></img>\r\n\t\t\t\t<span class=\"groupName\"></span>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"groupMembers\" style=\"display:none;\">\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t\r\n\t\t<!-- 聊天窗口  style=\"display:none;\" -->\r\n\t\t<div class=\"chatDiv\">\r\n\t\t\t<form action=\"\" method=\"post\">\r\n\t\t\t\t<fieldset>\r\n\t\t\t\t\t<legend>发送消息</legend>\r\n\t\t\t\t\t<table style=\"width:100%;\">\r\n\t\t\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t\t\t<td style=\"width: 80px;\" class=\"alignRight\">\r\n\t\t\t\t\t\t\t\t收件人：\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t\t\t<div contenteditable=\"false\"  class=\"messageToDiv divText\"></div>\r\n\t\t\t\t\t\t\t\t<span class=\"messageTo\" style=\"display: none;\" tabindex=\"0\"><span class=\"userNameSpan\"></span><span>; </span></span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t<td style=\"width: 80px;\" class=\"alignRight\">\r\n\t\t\t\t\t\t\t\t消息内容：\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t\t\t<textarea rows=\"10\" cols=\"62\" class=\"textArea\"></textarea>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t</table>\r\n\t\t\t\t</fieldset>\r\n\t\t\t</form>\r\n\t\t</div>\r\n\t</body>\r\n</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
