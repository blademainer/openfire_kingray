<%@ page import="java.util.*,
                 org.jivesoftware.openfire.XMPPServer,
                 org.jivesoftware.util.*,com.kingray.openfire.plugin.KingrayServicePlugin"
    errorPage="error.jsp"
%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>

<%-- Define Administration Bean --%>
<jsp:useBean id="admin" class="org.jivesoftware.util.WebManager"  />
<c:set var="admin" value="${admin.manager}" />
<% admin.init(request, response, session, application, out ); 
%>

<%  // Get parameters
    boolean save = request.getParameter("save") != null;
    boolean success = request.getParameter("success") != null;
    String secret = ParamUtils.getParameter(request, "secret");
    boolean enabled = ParamUtils.getBooleanParameter(request, "enabled");
    String allowedIPs = ParamUtils.getParameter(request, "allowedIPs");

    KingrayServicePlugin plugin = (KingrayServicePlugin) XMPPServer.getInstance().getPluginManager().getPlugin("kingrayplugin");

    // Handle a save
    Map errors = new HashMap();
    if (save) {
        if (errors.size() == 0) {
            plugin.setEnabled(enabled);
        	plugin.setSecret(secret);
            plugin.setAllowedIPs(StringUtils.stringToCollection(allowedIPs));
            response.sendRedirect("kingray-plugin.jsp?success=true");
            return;
        }
    }

    secret = plugin.getSecret();
    enabled = plugin.isEnabled();
    allowedIPs = StringUtils.collectionToString(plugin.getAllowedIPs());
%>

<html>
    <head>
        <title>用户服务属性</title>
        <meta name="pageID" content="kingray-plugin"/>
    </head>
    <body>


<p>
使用下面的表格来启用或禁用用户服务和配置密钥。
此服务默认是 <strong>关闭</strong>, 所以该服务默认是无效的。
</p>

<%  if (success) { %>

    <div class="jive-success">
    <table cellpadding="0" cellspacing="0" border="0">
    <tbody>
        <tr>
        	<td class="jive-icon"><img src="images/success-16x16.gif" width="16" height="16" border="0"></td>
        	<td class="jive-icon-label">
            	用户服务属性编辑成功！
        	</td>
        </tr>
    </tbody>
    </table>
    </div><br>
<% } %>

<form action="kingray-plugin.jsp?save" method="post">

<fieldset>
    <legend>用户服务</legend>
    <div>
    <p>
    	用户的添加、删除和编辑的通常在管理控制台的外部是不可用的。这项服务可以提供给外部应用使用http请求来控制用户信息。
	</p>

    <p>
    	然而,这种服务存在一个安全风险。因此，请求该服务必须提供一个有效的密钥。此外，你可以指定允许访问该服务的IP地址列表。一个空的列表意味着任何地址都能访问该服务。多个地址使用逗号进行分隔。
    </p>
    <ul>
        <input type="radio" name="enabled" value="true" id="rb01"
        <%= ((enabled) ? "checked" : "") %>>
        <label for="rb01"><b>启用</b> - 允许用户服务请求.</label>
        <br>
        <input type="radio" name="enabled" value="false" id="rb02"
         <%= ((!enabled) ? "checked" : "") %>>
        <label for="rb02"><b>关闭</b> - 拒绝用户服务请求.</label>
        <br><br>

        <label for="text_secret">密钥:</label>
        <input type="text" name="secret" value="<%= secret %>" id="text_secret">
        <br><br>

        <label for="text_secret">允许的IP地址:</label>
        <textarea name="allowedIPs" cols="40" rows="3" wrap="virtual"><%= ((allowedIPs != null) ? allowedIPs : "") %></textarea>
    </ul>
    </div>
</fieldset>

<br><br>

<input type="submit" value="保存修改">
</form>


</body>
</html>