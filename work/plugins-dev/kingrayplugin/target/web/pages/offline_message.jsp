<%@ page language="java"  contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*,
                 org.jivesoftware.openfire.XMPPServer,
                 org.jivesoftware.util.*,com.kingray.openfire.plugin.KingrayServicePlugin"
%>
<html>
	<head>
		<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
		<script	type="text/javascript">
		<!--
		$(function(){
			var offLineMessages = jQuery.parseJSON('<%=request.getSession().getAttribute("jsonOfflineMessages")%>');
			var messageTable = $("#messageTable");
			$(window).ready(function(){
				$.each(offLineMessages, function(i, offLineMessage){
					var tr = "<tr class='offlineMessage'  id='" + offLineMessage.messageDateTime.time + "'>";
					tr += "<td class='line-bottom-border messageTd'>";
					tr += offLineMessage.messageFrom;
					tr += "</td>";
					tr += "<td class='line-bottom-border messageTd'>";
					tr += offLineMessage.messageDateTimeStr;
					tr += "</td>";
					tr += "<td class='line-bottom-border messageTd'>";
					tr += offLineMessage.messageSubject;
					tr += "</td>";
					tr += "<td class='line-bottom-border messageTd'>";
					tr += offLineMessage.messageBody;
					tr += "</td>";
					tr += "</tr>";
					messageTable.append(tr);
				});
			});
			$(".messageTd").live("mouseover", function(){
				$(this).parent(".offlineMessage").children("td").removeClass("messageTd");
				$(this).parent(".offlineMessage").children("td").addClass("mouseOver");
				
			});
			$(".mouseOver").live("mouseleave", function(){
				$(this).parent(".offlineMessage").children("td").removeClass("mouseOver");
				$(this).parent(".offlineMessage").children("td").addClass("messageTd");
			});
		});
		-->
		</script>
		<style type="text/css">
		<!--
		#offlineMessageDiv{
			left: 100px;
		}
		table#messageTable tr td{
			text-align: left;
		}
		.table-header {
			text-align: left;
			font-family: verdana, arial, helvetica, sans-serif;
			font-size: 8pt;
			font-weight: bold;
			border-color: #ccc;
			border-style: solid;
			border-width: 1px 0 1px 0;
			padding: 5px;
		}
		tr {
			display: table-row;
			vertical-align: inherit;
			border-color: inherit;
		}
		.mouseOver{
			background-color: rgb(0, 156, 253);
		}
		.line-bottom-border {
			text-align: left;
			font-family: verdana, arial, helvetica, sans-serif;
			font-size: 9pt;
			border-color: #e3e3e3;
			border-style: solid;
			border-width: 0px 0px 1px 0px;
			padding: 5px;
		}
		.light-gray-border {
			border-color: #ccc;
			border-style: solid;
			border-width: 1px 1px 1px 1px;
			padding: 5px;
			-moz-border-radius: 3px;
		}
		table{
			border-spacing: 0px;
			border-top-width: 0px;
			border-right-width: 0px;
			border-bottom-width: 0px;
			border-left-width: 0px;
			width: 100%;
		}
		-->
		</style>
	</head>
	<body>
		<div id="offlineMessageDiv" class="light-gray-border">
			<table id="messageTable">
				<tr style="background:#eee;">
					<td width="10%;" class="table-header">
					发送人
					</td>
					<td width="20%;" class="table-header">
					发送时间
					</td>
					<td width="20%;" class="table-header">
					标题
					</td>
					<td width="50%;" class="table-header">
					内容
					</td>
				</tr>
		</div>
	</body>
</html>